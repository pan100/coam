<?php

require_once 'dao/DBConnection.php';
require_once 'model/Category.php';
/*
 * call the functions of this class to get all categories or specific categories by name
 * the controller then queries the database and returns either Category objects or arrays of Category objects
 */
class CategoryController {
	
	public static $instance;
	private $categories = array(); //an array of all categories
	
	private function __construct() {
		$connection = DBConnection::getInstance()->getConnection();
		//get all categories from the database
		$query = "SELECT * FROM categories;";
		$result = $connection->query($query);
		//store them in the $categories array
		while ($row = $result->fetch_assoc()) {
			$name = $row['name'];
			if(isset($row['description'])) {
				$description = $row['description'];
			}
			else {
				$description = null;
			}
			$tempCategory = new Category($name, $description);
			$this->categories[] = $tempCategory;
		}
	}
	
	public static function getInstance() {
		    if (!self::$instance)
   		 {
       		 self::$instance = new CategoryController();
    	}
    	
    	return self::$instance;
	}
	
	/*
	 * Returns all categories
	 */
	function getCategories() {
		return $this->categories;
	}
	
	/*
	 * Returns the number of posts in a category.
	 * @param - the Category object
	 */
	function numberOfPosts($category) {
		$connection = DBConnection::getInstance()->getConnection();
		
		$query = "SELECT 'post_id' FROM posts WHERE category = '" . mysql_real_escape_string($category->getName()) . "' AND status='POSTD';";
		return mysql_num_rows(mysql_query($query));
	}
	
	function getCategoryByName($categoryName) {
		//check if the categoryName consists of max 25 characters
		if(strlen($categoryName) <=25) {
			$connection = DBConnection::getInstance()->getConnection();
			$query = "SELECT *
						FROM categories
						WHERE name ='" . mysql_real_escape_string($categoryName) . "'";
			$result = mysql_query($query);
			//if no category with that name was found, return false
			if(mysql_num_rows($result) == 0) {
				return false;
			}
			else {
				$entity = mysql_fetch_array($result);
				$category = new Category($entity['name'], $entity['description']);
				return $category;
			}
		}
		//if the categoryName was too long, return false
		else {
			return false;
		}
	}
	
	function makeCategory($category) {
		if($category->getName() >25) {
			return false;
		}
		$connection = DBConnection::getInstance()->getConnection();
		$query = "INSERT INTO categories 
					VALUES (
					'"
					 . mysql_real_escape_string(strip_tags($category->getName()))
					 . "', '"
					 . mysql_real_escape_string(strip_tags($category->getDescription()))
					 . "', NULL
					);";
		if(mysql_query($query)) {
			return true;
		}
		else {
			echo mysql_error();
			return false;
		}
	}

}
?>