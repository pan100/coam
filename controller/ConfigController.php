<?php

/*
 * Class ConfigController
 * 
 * Singleton
 * 
 * Loads the configuration from the xml file and stores it in php variables. Allows administrators to perform changes
 * to the config, and saves it back to the file.
 * 
 * TODO - only getters are made so far, it is not possible for admins to change the config yet
 */
class ConfigController {
	private static $instance;
	
	private $configDOM;
	
	private $dbname;
	private $dbhost;
	private $dbuser;
	private $dbpass;
	
	private $sitetitle;
	private $sitesub;
	private $sitedescription;
	private $sitecontact;
	private $baseurl;
	
	private $commentsBeWrittenBy;
	private $commentsMustBeApproved;
	private $usersCanBeRegisteredBy;
	private $usersMustConfirmMail;
	private $design;
	private $summaryLength;
	
	private function __construct() {
		//make the DOMDoc
		$this->configDOM = new DOMDocument();
		//load the config file
		$this->configDOM->load('config/config.xml');
		//check if it is valid
		if(!($this->configDOM->validate())) {
			throw new Exception('configNotValid');
		}
		
		//load the xml into the variables
		$this->dbname = $this->configDOM->getElementsByTagName('name')->item(0)->nodeValue;
		$this->dbhost = $this->configDOM->getElementsByTagName('host')->item(0)->nodeValue;
		$this->dbuser = $this->configDOM->getElementsByTagName('user')->item(0)->nodeValue;
		$this->dbpass = $this->configDOM->getElementsByTagName('pass')->item(0)->nodeValue;
		
		$this->sitetitle = $this->configDOM->getElementsByTagName('title')->item(0)->nodeValue;
		$this->sitesub = $this->configDOM->getElementsByTagName('subtitle')->item(0)->nodeValue;
		$this->sitedescription = $this->configDOM->getElementsByTagName('description')->item(0)->nodeValue;
		$this->sitecontact = $this->configDOM->getElementsByTagName('contact')->item(0)->nodeValue;
		
		$this->commentsBeWrittenBy = $this->configDOM->getElementsByTagName('canBeWrittenBy')->item(0)->nodeValue;
		if($this->configDOM->getElementsByTagName('mustBeApproved')->item(0)->nodeValue == "yes") {
			$this->commentsMustBeApproved = true;
		}
		else if($this->configDOM->getElementsByTagName('mustBeApproved')->item(0)->nodeValue == "no") {
			$this->commentsMustBeApproved = false;
		}
		$this->usersCanBeRegisteredBy = $this->configDOM->getElementsByTagName('canBeRegisteredBy')->item(0)->nodeValue;
			if($this->configDOM->getElementsByTagName('mustConfirmMail')->item(0)->nodeValue == "yes") {
			$this->usersMustConfirmMail = true;
		}
		else if($this->configDOM->getElementsByTagName('mustConfirmMail')->item(0)->nodeValue == "no") {
			$this->usersMustConfirmMail = false;
		}
		$this->design = $this->configDOM->getElementsByTagName('design')->item(0)->nodeValue;
		$this->summaryLength = $this->configDOM->getElementsByTagName('numberOfCharactersInSummary')->item(0)->nodeValue;
		$this->baseurl = $this->configDOM->getElementsByTagName('baseurl')->item(0)->nodeValue;
	}
	
	/*
	 * getInstance - the function to call when the ConfigController is needed
	 */
	public static function getInstance() {
		    if (!self::$instance)
    {
        self::$instance = new ConfigController();
    }

    return self::$instance; 
	}
	
	function getDbname() {
		return $this->dbname;
	}
	function setDbname($dbname) {
		$this->dbname = $dbname;
		$this->configDOM->getElementsByTagName('name')->item(0)->nodeValue = $dbname;
	}
	
	function getDbhost() {
		return $this->dbhost;
	}
	
	
	function getDbuser() {
		return $this->dbuser;
	}
	
	function getDbpass() {
		return $this->dbpass;
	}
	
	function getSitetitle() {
		return $this->sitetitle;
	}
	
	function setSitetitle($sitetitle) {
		$this->sitetitle = $sitetitle;
		$this->configDOM->getElementsByTagName('title')->item(0)->nodeValue = $sitetitle;
	}
	
	function getSitesub() {
		return $this->sitesub;
	}
	
	function setSitesub($sitesub) {
		$this->sitesub = $sitesub;
		$this->configDOM->getElementsByTagName('subtitle')->item(0)->nodeValue = $sitesub;
	}
	
	function getSitedescription() {
		return $this->sitedescription;
	}
	
	function setSitedescription($sitedescription) {
		$this->sitedescription = $sitedescription;
		$this->configDOM->getElementsByTagName('description')->item(0)->nodeValue = $sitedescription;
	}
	
	function getSitecontact() {
		return $this->sitecontact;
	}
	
	function setSitecontact($sitecontact) {
		$this->sitecontact = $sitecontact;
		$this->configDOM->getElementsByTagName('contact')->item(0)->nodeValue = $sitecontact;
	}
	
	function getCommentsBeWrittenBy() {
		return $this->commentsBeWrittenBy;
	}
	
	function getCommentsMustBeApproved() {
		return $this->commentsMustBeApproved;
	}
	
	function getUsersCanBeRegisteredBy() {
		return $this->usersCanBeRegisteredBy;
	}
	
	function getUsersMustConfirmMail() {
		return $this->usersMustConfirmMail;
	}
	
	function getDesign() {
		return $this->design;
	}
	
	function getSummaryLength() {
		return $this->summaryLength;
	}
	
	function setSummaryLength($length) {
		$this->summaryLength = $length;
		$this->configDOM->getElementsByTagName('numberOfCharactersInSummary')->item(0)->nodeValue = $length;
	}
	
	function getBaseURL() {
		return $this->baseurl;
	}
	
	function saveChanges() {
		if($this->configDOM->validate()) {
			$this->configDOM->save('config/config.xml');
			return true;
		}
		else return false;
	}
}
?>