<?php

require_once 'model/Picture.php';
class PictureController {
	public static $instance;
	
	private function __construct() {
		
	}
	
	public static function getInstance() {
			    if (!self::$instance)
   		 {
       		 self::$instance = new PictureController();
    	}
    	
    	return self::$instance;
	}
	
	function getPicturesByPost($post) {
		$connection = DBConnection::getInstance()->getConnection();
		$query = "SELECT *
				FROM pictures
				JOIN posts_pictures ON pictures.filename = posts_pictures.pictureFile
				WHERE posts_pictures.post_id = '" . $post->getId() . "'";
		$result = mysql_query($query);
		
				$pictures = array();
		
		while($entity = mysql_fetch_array($result)) {
			$picture = new Picture($entity['filename'], $entity['name'], $entity['uploaded_by'], $entity['last_changed'], $entity['description']);
			$pictures[] = $picture;
		}
		
		return $pictures;
	}
	
	function addPicture($name, $description) {
		if(!isset($_FILES)) {
			return false;
		}
		
		$uploaddir = "pictures/" . $_SESSION['user'] . "/";
		$filename = basename($_FILES['picture']['name']);
		$uploadfile = $uploaddir . $filename;
		
		$filetype = substr($_FILES['picture']['name'], strrpos($_FILES['picture']['name'], '.') + 1);
		
		if($filetype != "jpg" && $filetype != "png" && $filetype != "gif") {
			echo "feiler her";
			echo $filetype;
			return false;
		}
		
		if(filesize($_FILES['picture']['tmp_name']) > 5000000) {
			return false;
		}
		if (!move_uploaded_file($_FILES['picture']['tmp_name'], $uploadfile)) {
  			  return false;
		}
		if($filetype == "jpg") $image = imagecreatefromjpeg($uploadfile);  
		if($filetype == "png") $image = imagecreatefrompng($uploadfile);
		if($filetype == "gif") $image = imagecreatefromgif($uploadfile);
		if (empty($image)) return false;
		
				//add the picture to the database
		$connection = DBConnection::getInstance()->getConnection();
		
		$query = "INSERT INTO pictures 
					VALUES (
					'"
					 . mysql_real_escape_string($name)
					 . "', '"
					 . mysql_real_escape_string($filename)
					 . "', '"
					 . mysql_real_escape_string($description)
					 . "', '"
					 . mysql_real_escape_string($_SESSION['user'])
					 . "', NOW()
					 
					);";
		if(!mysql_query($query)) {
			return false;
		}
		
		
		//make the thumbnails		
		$this->makeAndSaveThumb(150, "pictures/" . $_SESSION['user'] . "/thumbs/" . $filename, $image, $filetype);
		$this->makeAndSaveThumb(90, "pictures/" . $_SESSION['user'] . "/smallThumbs/" . $filename, $image, $filetype);
				imagedestroy($image);
		return true;
		
	}
	
	function getPictureByFilename($filename) {
				if(strlen($filename) <=30 ) {
			$connection = DBConnection::getInstance()->getConnection();
			$query = "SELECT *
						FROM pictures
						WHERE filename ='" . mysql_real_escape_string($filename) . "';";
			$result = mysql_query($query);
			
		if(mysql_num_rows($result) == 0) {
				return false;
			}
			else {
				$entity = mysql_fetch_array($result);
				//$name, $pic, $email, $isConfirmed, $role, $createdAt
				$picture = new Picture($entity['filename'], $entity['name'], $entity['uploaded_by'], 
				$entity['last_changed'], $entity['description']);
				return $picture;
			}
		}
		else return false;
	}
	
	function getPicturesByUploader($username) {
		$connection = DBConnection::getInstance()->getConnection();
		$query = " SELECT *
					FROM pictures
					WHERE uploaded_by = '" . mysql_real_escape_string($username) . "'
					ORDER BY last_changed DESC;";
		$result = mysql_query($query);
		
		$pictures = array();
		if(mysql_error()) {
			echo mysql_error();
		}
		while($entity = mysql_fetch_array($result)) {
			$picture = new Picture($entity['filename'], $entity['name'], $entity['uploaded_by'], $entity['last_changed'], $entity['description']);
			$pictures[] = $picture;
		}
		
		return $pictures;	
	}
	
	private function makeAndSaveThumb($thumbsize, $savePath, $image, $filetype) {
		$imagewidth = imagesx($image);
		$imageheight = imagesy($image); 
		if ($imagewidth >= $imageheight) {
		  $thumbwidth = $thumbsize;
		  $factor = $thumbsize / $imagewidth;
		  $thumbheight = $imageheight * $factor;
		}
		if ($imageheight >= $imagewidth) {
		  $thumbheight = $thumbsize;
		  $factor = $thumbsize / $imageheight;
		  $thumbwidth = $imagewidth * $factor;
		}
		$thumb = @imagecreatetruecolor($thumbwidth,$thumbheight);
		imagecopyresized($thumb, $image, 0, 0, 0, 0, $thumbwidth, $thumbheight, $imagewidth, $imageheight);
		if($filetype == "jpg") {
			imagejpeg($thumb, $savePath);
		}
		else if($filetype == "png"){
			imagepng($thumb, $savePath);
		}
		else {
			imagegif($thumb, $savePath);
		}

		imagedestroy($thumb); 
	}
}
?>