<?php
require_once 'dao/DBConnection.php';
require_once 'controller/ConfigController.php';
require_once 'model/Post.php';

class PostController {
	public static $instance;
	
	private function __construct() {
		
	}
	
	public static function getInstance() {
			    if (!self::$instance)
   		 {
       		 self::$instance = new PostController();
    	}
    	
    	return self::$instance;
	}
	
	/*
	 * Rturn the two newest in a category.
	 */
	function getTwoNewestInCategory($category) {
		$connection = DBConnection::getInstance()->getConnection();
		$query = " SELECT *
					FROM posts
					WHERE STATUS = 'POSTD'
					AND category = '" . $category->getName() . "'
					ORDER BY created_at DESC
					LIMIT 2;";
		$result = mysql_query($query);
		
		$twoNewest = array();
		
		while($entity = mysql_fetch_array($result)) {
			$post = new Post($entity['post_id'], $entity['username'], $entity['status'], $entity['title'], 
			$entity['content'], $entity['created_at'], $entity['edited_at'], $entity['edited_by'], $entity['preferredPicture'],
			$entity['category']);
			$twoNewest[] = $post;
		}
		
		return $twoNewest;
	}
	
	/*
	 * Return all posts in a category
	 */
	function getPostsInCategory($category) {
		$connection = DBConnection::getInstance()->getConnection();
		$query = " SELECT *
					FROM posts
					WHERE STATUS = 'POSTD'
					AND category = '" . $category->getName() . "'
					ORDER BY created_at DESC;";
		$result = mysql_query($query);
		
		$posts = array();
		
		while($entity = mysql_fetch_array($result)) {
			$post = new Post($entity['post_id'], $entity['username'], $entity['status'], $entity['title'], 
			$entity['content'], $entity['created_at'], $entity['edited_at'], $entity['edited_by'], $entity['preferredPicture'],
			$entity['category']);
			$posts[] = $post;
		}
		
		return $posts;		
	}
	
	function getPostsByUsername($username) {
		$connection = DBConnection::getInstance()->getConnection();
		$query = " SELECT *
					FROM posts
					WHERE username = '" . mysql_real_escape_string($username) . "'
					ORDER BY created_at DESC;";
		$result = mysql_query($query);
		
		$posts = array();
		if(mysql_error()) {
			echo mysql_error();
		}
		while($entity = mysql_fetch_array($result)) {
			$post = new Post($entity['post_id'], $entity['username'], $entity['status'], $entity['title'], 
			$entity['content'], $entity['created_at'], $entity['edited_at'], $entity['edited_by'], $entity['preferredPicture'],
			$entity['category']);
			$posts[] = $post;
		}
		
		return $posts;	
	}
	
	function getPostedPostsByUsername($username) {
		$connection = DBConnection::getInstance()->getConnection();
		$query = " SELECT *
					FROM posts
					WHERE STATUS = 'POSTD'
					AND author = '" . mysql_real_escape_string($username) . "'
					ORDER BY created_at DESC;";
		$result = mysql_query($query);
		
		$posts = array();
		
		while($entity = mysql_fetch_array($result)) {
			$post = new Post($entity['post_id'], $entity['username'], $entity['status'], $entity['title'], 
			$entity['content'], $entity['created_at'], $entity['edited_at'], $entity['edited_by'], $entity['preferredPicture'],
			$entity['category']);
			$posts[] = $post;
		}
		
		return $posts;	
	}
	
	/*
	 * Get a post by the id of the post. Returns false if there is no such post.
	 */
	function getPostById($id) {
		//make sure the $id is valid
		if(is_numeric($id) && $id < 100000000) {
			$connection = DBConnection::getInstance()->getConnection();
			$query = "SELECT *
						FROM posts
						WHERE post_id ='" . $id . "'";
			$result = mysql_query($query);
			//if no post with that id was found, return false
			if(mysql_num_rows($result) == 0) {
				return false;
			}
			else {
				$entity = mysql_fetch_array($result);
				$post = new Post($entity['post_id'], $entity['username'], $entity['status'], $entity['title'], 
				$entity['content'], $entity['created_at'], $entity['edited_at'], $entity['edited_by'], $entity['preferredPicture'],
				$entity['category']);
				return $post;
			}
		}
		//if the $id was not valid we return false, and we don't even bother the DB
		else return false;
	}
	//enten pictureFile eller NULL
	function newPost($title, $content, $status, $picture, $user, $category) {

			$connection = DBConnection::getInstance()->getConnection();
			
			$title = strip_tags($title);
			$content = strip_tags($content);
		$query = "INSERT INTO posts (
			post_id ,
			username ,
			status ,
			title ,
			content ,
			created_at ,
			edited_at ,
			edited_by ,
			preferredPicture ,
			category
			)
			VALUES (
			NULL , '" . $user . "', '" . $status . "', '" . $title . "', '" . $content . "', NOW( ) ,
			CURRENT_TIMESTAMP , NULL , ";
		
		$appendToQuery = " , '" . $category . "');";
		if($picture != null) {
			$query .= "'" . $picture . "'";
		}
		else $query .= "NULL";
		$query .= $appendToQuery;
		
		if(mysql_query($query)) {
			return true;
		}
		else {
			echo mysql_error();
			return false;
		}
	}
	
	function searchInPosts($searchString) {
		$postsToReturn = array();
		$idsInResult = array();
		$connection = DBConnection::getInstance()->getConnection();
		
		//search
		$searchString = trim($searchString);
		if($searchString == "") {
			return array();
		}
		$query = "select * from posts where title like '%" . mysql_real_escape_string($searchString) . "%' 
		OR content like '%" . $searchString .  "%' OR category like '%" . $searchString . "%' LIMIT 10;";
		$result = mysql_query($query);
		
		
		
		while($entity = mysql_fetch_array($result)) {
							$post = new Post($entity['post_id'], $entity['username'], $entity['status'], $entity['title'], 
				$entity['content'], $entity['created_at'], $entity['edited_at'], $entity['edited_by'], $entity['preferredPicture'],
				$entity['category']);
			$postsToReturn[] = $post;
		}
		
		//return the results
		return $postsToReturn;
	}
	
	function updatePost($postToAlter) {
				//get the post to edit from the base
		if(!($post = $this->getPostById($postToAlter->getId()))) {
			return false;
		}
		$firstQueryPart = "UPDATE `posts` SET ";
		//check each field - if there are changes, add them to the query
		$changesMade = false;
		if($post->getCategory() != $postToAlter->getCategory()) {
			$firstQueryPart .= "category = '" . $postToAlter->getCategory() . "', ";
			$changesMade = true;
		}
		
		if($post->getContent() != $postToAlter->getContent()) {
			$firstQueryPart .= "content = '" . $postToAlter->getContent() . "', ";
			$changesMade = true;
		}
		if($post->getPicture() != $postToAlter->getPicture()) {
			$firstQueryPart .= "preferredPicture = '" . $postToAlter->getPicture() . "', ";
			$changesMade = true;
		}
		if($post->getStatus() != $postToAlter->getStatus()) {
			$firstQueryPart .= "status = '" . $postToAlter->getStatus() . "', ";
			$changesMade = true;
		}
			if($post->getTitle() != $postToAlter->getTitle()) {
			$firstQueryPart .= "title = '" . $postToAlter->getTitle() . "', ";
			$changesMade = true;
		}
		
		if(!($changesMade)) {

			return false;
		}
		
		$lastQueryPart = "edited_by = '" . $_SESSION['user'] . "' WHERE post_id = '" . $postToAlter->getId() . "' LIMIT 1 ;";
		$query = $firstQueryPart . $lastQueryPart;
		//run the sql query
		$connection = DBConnection::getInstance()->getConnection();
		mysql_query($query);
		if(mysql_error()) {
			echo $query;
			echo mysql_error();
		}
		return true;
	}
	
	function getTenNewestPosts() {
				$connection = DBConnection::getInstance()->getConnection();
		$query = " SELECT *
					FROM posts
					WHERE STATUS = 'POSTD' 
					ORDER BY created_at DESC
					LIMIT 10;";
		$result = mysql_query($query);
		
		$tenNewest = array();
		
		while($entity = mysql_fetch_array($result)) {
			$post = new Post($entity['post_id'], $entity['username'], $entity['status'], $entity['title'], 
			$entity['content'], $entity['created_at'], $entity['edited_at'], $entity['edited_by'], $entity['preferredPicture'],
			$entity['category']);
			$tenNewest[] = $post;
		}
		
		return $tenNewest;
	}
}
?>