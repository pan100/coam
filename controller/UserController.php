<?php

require_once 'model/User.php';
class UserController {
	
public static $instance;
	
	private function __construct() {
		
	}
	
	public static function getInstance() {
			    if (!self::$instance)
   		 {
       		 self::$instance = new UserController();
    	}
    	
    	return self::$instance;
	}
	
	function getUserByUserName($userName) {
		if(strlen($userName) <=25 ) {
			$connection = DBConnection::getInstance()->getConnection();
			$query = "SELECT *
						FROM users
						WHERE username ='" . mysql_real_escape_string($userName) . "';";
			$result = mysql_query($query);
			
		if(mysql_num_rows($result) == 0) {
				return false;
			}
			else {
				$entity = mysql_fetch_array($result);
				//$name, $pic, $email, $isConfirmed, $role, $createdAt
				$user = new User($entity['username'], $entity['pictureFile'], $entity['email'], $entity['isConfirmed'], 
				$entity['role_id'], $entity['created_at'], $entity['firstname'], $entity['lastname']);
				return $user;
			}
		}
		else return false;
	}
	
	function userExists($userName) {
				if(strlen($userName) <=25 ) {
			$connection = DBConnection::getInstance()->getConnection();
			$query = "SELECT * FROM users
						WHERE username ='" . mysql_real_escape_string($userName) . "';";
			$result = mysql_query($query);
			
			
		if(mysql_num_rows($result) == 0) {
				return false;
			}
			else {
				return true;
			}
		}
		else return false;
	}
	
	function emailExists($email) {
				if(strlen($email) <=40 ) {
			$connection = DBConnection::getInstance()->getConnection();
			$query = "SELECT *
						FROM users
						WHERE email ='" . mysql_real_escape_string($email) . "';";
			$result = mysql_query($query);
			
		if(mysql_num_rows($result) == 0) {
				return false;
			}
			else {
				return true;
			}
		}
		else return false;		
	}
	
	/*
	 * Login - returns false if the login failed, else it sets the user and userRole fields in the session variable
	 * and returns true.
	 */
	function login($userName, $password) {
		if(strlen($userName) <=25) {
			//username is of correct length.
			$connection = DBConnection::getInstance()->getConnection();
			$query = "SELECT *
						FROM users
						WHERE isConfirmed = '1' AND username ='" . mysql_real_escape_string($userName) . "'
						 AND password ='" . md5($password) . "';";
			$result = mysql_query($query);
			if(mysql_num_rows($result) == 1) {
				//log the user in
				$entity = mysql_fetch_array($result);
				
				$_SESSION['user'] = $entity['username'];
				$_SESSION['userRole'] = $entity['role_id'];
				return true;
			}
			else {
				return false;
			}
						
		}
		else {
			//username is too long.
			return false;			
		}
		
	}
	
	function validateUser($userName, $password) {
			if(strlen($userName) <=25) {
			//username is of correct length.
			$connection = DBConnection::getInstance()->getConnection();
			$query = "SELECT *
						FROM users
						WHERE username ='" . mysql_real_escape_string($userName) . "'
						 AND password ='" . md5($password) . "';";
			$result = mysql_query($query);
			if(mysql_num_rows($result) == 1) {
				//the username and password matches
				return true;
			}
			else return false;
						
		}
		else {
			//username is too long.
			return false;			
		}		
	}
	
	function setPassword($username, $password) {

			$connection = DBConnection::getInstance()->getConnection();
			$query = "UPDATE `coam-cms`.`users` SET `password` = '" . md5($password) . "'
			WHERE CONVERT( `users`.`username` USING utf8 ) = '" . $username . "' LIMIT 1 ;";
			mysql_query($query);
	}
	
	function updateUser($alteredUserObject) {
		//get the user to edit from the base
		if(!($user = $this->getUserByUserName($alteredUserObject->getUserName()))) {
			return false;
		}
		$firstQueryPart = "UPDATE `users` SET ";
		//check each field - if there are changes, add them to the query
		$changesMade = false;
		if($user->getFirstName() != $alteredUserObject->getFirstName()) {
			$firstQueryPart .= "firstname = '" . $alteredUserObject->getFirstName() . "'";
			$changesMade = true;
		}
		
		if($user->getLastName() != $alteredUserObject->getLastName()) {
			$firstQueryPart .= "lastname = '" . $alteredUserObject->getLastName() . "'";
			$changesMade = true;
		}
		
		if($user->getRole() != $alteredUserObject->getRole()) {
			$firstQueryPart .= "role_id = '" . $alteredUserObject->getRole() . "'";
			$changesMade = true;
		}
		
		if($user->getEmail() != $alteredUserObject->getEmail()) {
			$firstQueryPart .= "email = '" . $alteredUserObject->getEmail() . "'";
			$changesMade = true;
		}

		if($user->getPic() != $alteredUserObject->getPic()) {
			$firstQueryPart .= "pictureFile = '" . $alteredUserObject->getPic() . "'";
			$changesMade = true;
		}
		
		if($user->isConfirmed() != $alteredUserObject->isConfirmed()) {
			if($alteredUserObject->isConfirmed()) {
				$firstQueryPart .= "isConfirmed = '" . "1" . "'";

			}
			else {
				$firstQueryPart .= "isConfirmed = '" . "1" . "'";
			}
			$changesMade = true;
		}
		
		if(!($changesMade)) {

			return false;
		}
		
		$lastQueryPart = " WHERE CONVERT( `users`.`username` USING utf8 ) = '" . $alteredUserObject->getUserName() . "' LIMIT 1 ;";
		$query = $firstQueryPart . $lastQueryPart;
		//run the sql query
		$connection = DBConnection::getInstance()->getConnection();
		mysql_query($query);
		if(mysql_error()) {
			echo mysql_error();
		}
		return true;
		
	}
	
	function newUser($user) {
		$password = md5($_POST['password']);
		$query = "INSERT INTO users (
`username` ,
`role_id` ,
`firstname` ,
`lastname` ,
`email` ,
`password` ,
`isConfirmed` ,
`pictureFile` ,
`created_at`
)
VALUES (
'" . $user->getUserName() . "', '" . $user->getRole() . "', '" . 
$user->getFirstName() . "', '" . $user->getLastName() . "', '" . $user->getEmail() . "', '" . 
$password . "', '0', NULL , NOW( )
);";

		mysql_query($query);
		
		if(!file_exists('pictures/' . $user->getUserName() . '/')) 
{ 
mkdir("pictures/" . $user->getUserName() . '/'); 
mkdir("pictures/" . $user->getUserName() . '/thumbs/'); 
mkdir("pictures/" . $user->getUserName() . '/smallThumbs/'); 
}
	}
	
	function getAllUsers() {
		
			$connection = DBConnection::getInstance()->getConnection();
			$query = "SELECT *
						FROM users;";
			$result = mysql_query($query);
			
			$users=array();
			

				while($entity = mysql_fetch_array($result)) {
									$user = new User($entity['username'], $entity['pictureFile'], $entity['email'], $entity['isConfirmed'], 
				$entity['role_id'], $entity['created_at'], $entity['firstname'], $entity['lastname']);
				$users[] = $user;
				}
				
				return $users;
				//$name, $pic, $email, $isConfirmed, $role, $createdAt

		
	
	}
	
}

?>