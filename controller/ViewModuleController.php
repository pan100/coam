<?php
require_once 'view/modules/Index.php';
require_once 'view/modules/ReadPost.php';
require_once 'view/modules/UserInfo.php';
require_once 'view/modules/ShowCategories.php';
require_once 'view/modules/ShowCategory.php';
require_once 'view/modules/Login.php';
require_once 'view/modules/EditUser.php';
require_once 'view/modules/WritePost.php';
require_once 'view/modules/Search.php';
require_once 'view/modules/UploadPicture.php';
require_once 'view/modules/ShowPicture.php';
require_once 'view/modules/EditPost.php';
require_once 'view/modules/MyPosts.php';
require_once 'view/modules/ConfigureCOAM.php';
require_once 'view/modules/MyPictures.php';
require_once 'view/modules/EditPicture.php';
require_once 'view/modules/AdministratorMenu.php';
require_once 'view/modules/AdministrateUsers.php';
require_once 'view/modules/NewUser.php';


	/*
	 * POSSIBLE MODULES:
	 * -----------------
	 * 		Available for all:
	 * 
	 * Index - this module should not be specified, will be used if nothing is specified
	 * ReadPost - outputs one single post, with everything needed - requires post id
	 * UserInfo - information about a user - requires user name
	 * ShowCategories - will output all categories, and should output all categories and subcategories
	 * ShowCategory - all posts within one category - requires category name
	 * ShowPicture
	 * CategoryInfo - output on only one category - requires category name
	 * WriteComment - only available if the settings for COAM allow nonregistered users to comment
	 * Search - requires a search phraze at minimum, if not the design should show an advanced search form
	 * NewUser - to register new users
	 * Login - log in if you are already registered
	 * 
	 * 		Only logged in users:
	 * 
	 * WritePost - write a new post
	 * EditPost - edit an existing post - requires post id
	 * EditUser
	 * EditComment - edit a comment - requires the id of the post
	 * MyPosts - administrate the user's own posts
	 * MyPictures - show and administrate pictures uploaded by the users
	 * UploadPicture - lets you upload a picture and select if it should be your avatar or if it should be associated to
	 * a specific post, and if it should be the main image of the post
	 * AdministrateCategories - gives the user possibilities to 
	 * 
	 * 		Only administrator users:
	 * 
	 * ConfigureCOAM - change the COAM config file without editing it directly
	 * CommentsAwaitingModeration - not in this version
	 * AdministrateUsers - for now, it just gives a list of users with their confirmation status
	 * AdministratorMenu
	 */

class ViewModuleController {
	
	public static function getCorrectModule($DOMDocument) {
		if(isset($_GET['module'])) {
			if($_GET['module'] == 'Index'){
				return new Index($DOMDocument);				
			}
			else if($_GET['module'] == 'ReadPost'){
				return new ReadPost($DOMDocument);				
			}
			else if($_GET['module'] == 'UserInfo') {
				return new UserInfo($DOMDocument);
			}
			else if($_GET['module'] == 'ShowCategory') {
				return new ShowCategory($DOMDocument);
			}
			else if($_GET['module'] == 'Login') {
				return new Login($DOMDocument);
			}
			else if($_GET['module'] == 'EditUser') {
				return new EditUser($DOMDocument);
			}
			else if($_GET['module'] == 'WritePost') {
				return new WritePost($DOMDocument);
			}
			else if($_GET['module'] == 'Search') {
				return new Search($DOMDocument);
			}
			else if($_GET['module'] == 'UploadPicture') {
				return new UploadPicture($DOMDocument);
			}
			else if($_GET['module'] == 'ShowPicture') {
				return new ShowPicture($DOMDocument);
			}
			else if($_GET['module'] == 'EditPost') {
				return new EditPost($DOMDocument);
			}
			else if($_GET['module'] == 'MyPosts') {
				return new MyPosts($DOMDocument);
			}
			else if($_GET['module'] == 'ConfigureCOAM') {
				return new ConfigureCOAM($DOMDocument);
			}
			else if($_GET['module'] == 'MyPictures') {
				return new MyPictures($DOMDocument);
			}			
			else if($_GET['module'] == 'EditPicture') {
				return new EditPicture($DOMDocument);
			}
			else if($_GET['module'] == 'AdministratorMenu') {
				return new AdministratorMenu($DOMDocument);
			}
			else if($_GET['module'] == 'AdministrateUsers') {
				return new AdministrateUsers($DOMDocument);
			}
			else if($_GET['module'] == 'NewUser') {
				return new NewUser($DOMDocument);
			}			
			else return new Index($DOMDocument);
		}
		else {
			return new Index($DOMDocument);
		}
	}
}
?>