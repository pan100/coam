<?php
/*CREATE TABLE `users` (
`username` VARCHAR( 25 ) NOT NULL ,
`role_id` VARCHAR( 6 ) NOT NULL ,
`firstname` VARCHAR( 30 ) NOT NULL ,
`lastname` VARCHAR( 30 ) NOT NULL ,
`email` VARCHAR( 40 ) NOT NULL ,
`password` VARCHAR( 32 ) NOT NULL COMMENT 'md5',
`created_at` TIMESTAMP NOT NULL ,
PRIMARY KEY ( `username` ) ,
UNIQUE (
`email`
)
);

CREATE TABLE `categories` (`name` VARCHAR( 25 ) NOT NULL ,
`parent` VARCHAR( 25 ) NOT NULL ,
`description` TINYTEXT NULL ,
PRIMARY KEY ( `name` , `parent` )
);


CREATE TABLE `posts` (
  `post_id` int(8) NOT NULL auto_increment,
  `username` varchar(25) NOT NULL,
  `status` varchar(5) NOT NULL,
  `title` varchar(50) NOT NULL,
  `content` text NOT NULL,
  `created_at` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `edited_at` timestamp NOT NULL default '0000-00-00 00:00:00',
  `edited_by` varchar(25) NOT NULL COMMENT 'username',
  PRIMARY KEY  (`post_id`)
);

CREATE TABLE `post_categories` (
`post_id` INT( 8 ) NOT NULL ,
`category` VARCHAR( 25 ) NOT NULL ,
PRIMARY KEY ( `post_id` , `category` )
);

CREATE TABLE `comments` (
`comment_id` INT( 8 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`edited_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
`approved_at` TIMESTAMP NULL ,
`approved` ENUM( '0', '1' ) NOT NULL ,
`approvedBy` VARCHAR( 25 ) NULL ,
`comment` TEXT NOT NULL ,
`username` VARCHAR( 25 ) NULL ,
`nonusername` VARCHAR( 25 ) NULL ,
`nonusermail` VARCHAR( 50 ) NULL
);

INSERT INTO `users` (`username`, `role_id`, `firstname`, `lastname`, `email`, `password`, `created_at`) VALUES ('test', 'USER', 'Ola', 'Dunk', 'ola@dunk.com', '1a1dc91c907325c69271ddf0c944bc72', CURRENT_TIMESTAMP);

?>