<?php
require_once 'controller/ConfigController.php';

class DBConnection {
	
	public static $instance;
	private $connection;
	
	private function __construct() {
		
		$dbhost = ConfigController::getInstance()->getDbhost();
		$dbuser = ConfigController::getInstance()->getDbuser();
		$dbpass = ConfigController::getInstance()->getDbpass();
		$dbname = ConfigController::getInstance()->getDbname();
		
		$this->connection = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname) or die                      ('Error connecting to mysql');
//		mysql_set_charset('utf8');
		$query = "SET NAMES 'utf8';";
		$this->connection->query($query);
		
		
	}
	
	public static function getInstance() {
		    if (!self::$instance)
    {
        self::$instance = new DBConnection();
    }

    return self::$instance; 
	}
	
	public function getConnection() {
		return $this->connection;
	}
	
   function __destruct() {
   		mysql_close($this->connection);
   }
}
?>