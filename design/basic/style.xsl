<?xml version="1.0" encoding="utf-8"?><!-- DWXMLSource="sample.xml" --><!DOCTYPE xsl:stylesheet  [
	<!ENTITY nbsp   "&#160;">
	<!ENTITY copy   "&#169;">
	<!ENTITY reg    "&#174;">
	<!ENTITY trade  "&#8482;">
	<!ENTITY mdash  "&#8212;">
	<!ENTITY ldquo  "&#8220;">
	<!ENTITY rdquo  "&#8221;"> 
	<!ENTITY pound  "&#163;">
	<!ENTITY yen    "&#165;">
	<!ENTITY euro   "&#8364;">
]>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns="http://www.w3.org/1999/xhtml"
                version="1.0" >

<xsl:output method="html"
            media-type="text/html"
            doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
            doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
            indent="yes"
            encoding="UTF-8" />
<xsl:template match="/">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title><xsl:value-of select="coam/title"/></title>
<link href="design/basic/style.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<xsl:comment>
.style1 {color: #FF0000}
</xsl:comment>
</style>
</head>

<body>
<table id="Table_01" style="width:90%" border="0" cellpadding="7" cellspacing="0">
  <tr>
    <td colspan="2" rowspan="2" style="width:225px"><img src="design/basic/images/Untitled-1_01.gif" width="209" height="199" alt="COAM-CMS Logo" /></td>
    <td class="titleTd" colspan="2" align="center" valign="middle"><p class="titleStyle"><xsl:value-of select="coam/title"/></p></td>
  </tr>
  <tr>
    <td colspan="2" align="center" valign="middle"><p class="subTitleStyle">- <xsl:value-of select="coam/subtitle"/><br />
    </p>
        <p class="contactInfo">Kontaktinfo: <xsl:value-of select="coam/contact"/></p></td>
  </tr>
  <tr>
    <td width="171" valign="top" id="leftMenu" style="width:15%">
    <xsl:choose>
<xsl:when test="coam/content/pageSpecific/module/Index">
</xsl:when>
<xsl:otherwise>
<a href="index.php">Tilbake til hovedside</a>
</xsl:otherwise>
</xsl:choose><p></p>
        <form action="index.php" method="get" id="form1">
          <p>
            <input type="text" name="search" id="search" />
            <input name="send" type="submit" value="Søk..." />
            </p>
          <p>Bruk tegnet % som wildcard
            <input type="hidden" name="module" id="module" value="Search"/>
          </p>
        </form>
        <p><a href="?module=ShowCategories">Kategorier:</a></p>
        <ul>
        <xsl:for-each select="coam/content/menu/menuCategory">
          <li><a><xsl:attribute name="href">?module=ShowCategory&amp;category=<xsl:value-of select="@name" /></xsl:attribute>
<xsl:value-of select="@name"/> (<xsl:value-of select="@numberOfPosts"/>)</a></li>
                  </xsl:for-each>
        </ul>

        <p><a href="rss.php">RSS</a></p>
        <p>&nbsp;</p></td>
    <td colspan="2" valign="top" style="width:70%">
    <!--This is where the content from the pageSpecific is inserted. First, we have the title of the module.-->
    <xsl:if test="coam/content/pageSpecific/module/Index">
    <h1>Hovedside</h1>
    </xsl:if>
    
        <xsl:if test="coam/content/pageSpecific/module/ReadPost">
    <h1>Visning av post<xsl:if test="coam/content/pageSpecific/module/ReadPost/post">
       - &quot;<xsl:value-of select="coam/content/pageSpecific/module/ReadPost/post/@title"/>&quot;
    </xsl:if></h1>
    </xsl:if>
    
            <xsl:if test="coam/content/pageSpecific/module/UserInfo">
    <h1>Informasjon om bruker <xsl:if test="coam/content/pageSpecific/module/UserInfo/user">
       - &quot;<xsl:value-of select="coam/content/pageSpecific/module/UserInfo/user/@userName"/>&quot;
    </xsl:if></h1>
    </xsl:if>
    
          <xsl:if test="coam/content/pageSpecific/module/ShowCategories">
    <h1>Kategorier</h1>
    </xsl:if>
    
          <xsl:if test="coam/content/pageSpecific/module/ShowCategory">
    <h1>Kategori<xsl:if test="coam/content/pageSpecific/module/ShowCategory/@title">
       - &quot;<xsl:value-of select="coam/content/pageSpecific/module/ShowCategory/@title"/>&quot;
    </xsl:if></h1>
    </xsl:if>
    
              <xsl:if test="coam/content/pageSpecific/module/EditUser">
    <h1>Redigere bruker<xsl:if test="coam/content/pageSpecific/module/EditUser/user">
       - &quot;<xsl:value-of select="coam/content/pageSpecific/module/EditUser/user/@userName"/>&quot;
    </xsl:if></h1>
    </xsl:if>
    
          <xsl:if test="coam/content/pageSpecific/module/Search">
    <h1>Søk</h1>
    </xsl:if>
    
          <xsl:if test="coam/content/pageSpecific/module/ShowPicture">
    <h1>Visning av bilde</h1>
    </xsl:if>
    
          <xsl:if test="coam/content/pageSpecific/module/NewUser">
    <h1>Registrere ny bruker</h1>
    </xsl:if>
    
          <xsl:if test="coam/content/pageSpecific/module/Login">
    <h1>Innlogging</h1>
    </xsl:if>
    
          <xsl:if test="coam/content/pageSpecific/module/WritePost">
    <h1>Skriv en post</h1>
    </xsl:if>
    
          <xsl:if test="coam/content/pageSpecific/module/EditPost">
    <h1>Rediger post</h1>
    </xsl:if>
    
          <xsl:if test="coam/content/pageSpecific/module/MyPosts">
    <h1 align="center">Dine poster</h1>
    </xsl:if>
    
          <xsl:if test="coam/content/pageSpecific/module/MyPictures">
    <h1>Dine bilder</h1>
    </xsl:if>
    
          <xsl:if test="coam/content/pageSpecific/module/UploadPicture">
    <h1>Opplasting av bilde</h1>
    </xsl:if>
    
          <xsl:if test="coam/content/pageSpecific/module/EditPicture">
    <h1>Rediger bilde</h1>
    </xsl:if>
    
          <xsl:if test="coam/content/pageSpecific/module/ConfigureCOAM">
    <h1>Konfigurasjon av COAM-CMS</h1>
    </xsl:if>
    
          <xsl:if test="coam/content/pageSpecific/module/AdministrateUsers">
    <h1>Administrasjon av brukere</h1>
    </xsl:if>
    
          <xsl:if test="coam/content/pageSpecific/module/AdministratorMenu">
    <h1>Administratormeny</h1>
    <!-- Note to self: remember that admins can add categories, confirm users, etc-->
    </xsl:if>
    
    <!-- Then, if there are errorMessages, insert them.-->
    <xsl:if test="coam/content/pageSpecific/errorMessage">
      <img src="design/basic/images/fail.jpg" alt="Feilmelding" width="250" height="224" />
      <br/>
    <!-- ReadPost can give us:
    type= idNotSpecified
    type=noSuchPost additionalInfo= [the id that was provided]
    -->
   		<xsl:if test="coam/content/pageSpecific/errorMessage/@type = 'idNotSpecified'">feil: du har ikke spesifisert noe id!</xsl:if>
    	<xsl:if test="coam/content/pageSpecific/errorMessage/@type = 'noSuchPost'">det finnes ikke noe post med id <xsl:value-of select="coam/content/pageSpecific/errorMessage/@additionalInfo" /></xsl:if>
     <!-- UserInfo can give us:
    type= userNotSpecified
    type=noSuchUser additionalInfo= [the username that was provided]
    -->
    	<xsl:if test="coam/content/pageSpecific/errorMessage/@type = 'userNotSpecified'">feil: du har ikke spesifisert noe brukernavn!</xsl:if>
    	<xsl:if test="coam/content/pageSpecific/errorMessage/@type = 'noSuchPost'">det finnes ikke noe bruker med brukernavnet <xsl:value-of select="coam/content/pageSpecific/errorMessage/@additionalInfo" /></xsl:if>
    	<!-- ShowCategory can give us:
        type= categoryNotSpecified
        type= noSuchCategory
        additionalInfo= [the category specified by the user]
        type= notAuthorized
        additionalInfo= makePosts
        type=categoryMakingFailed-->
    	<xsl:if test="coam/content/pageSpecific/errorMessage/@type = 'categoryNotSpecified'">feil: du har ikke spesifisert noe kategori!		
        </xsl:if>
        <xsl:if test="coam/content/pageSpecific/errorMessage/@type = 'noSuchCategory'">det finnes ikke noe kategori med navnet <xsl:value-of select="coam/content/pageSpecific/errorMessage/@additionalInfo" /></xsl:if> 
        <xsl:if test="coam/content/pageSpecific/errorMessage/@type = 'notAuthorized'">Du er ikke autorisert til å vise denne siden eller utføre den handlingen du forsøkte å utføre</xsl:if>     
   </xsl:if>
    	  <xsl:if test="coam/content/pageSpecific/errorMessage/@type = 'categoryMakingFailed'">feil under opprettelse av kategori. Kanskje du ikke oppgav en beskrivelse, eller kanskje navnet var over 25 tegn? Det kan også ha vært databasen som feilet.
        </xsl:if>
        	<!-- Login can give us there errorMessages:
            type= noPasswordProvided
            type=wrongPassword
            type=noSuchUser
            type=notConfirmed-->
           		 <xsl:if test="coam/content/pageSpecific/errorMessage/@type = 'noPasswordProvided'">Du skrev ikke inn noe passord
        </xsl:if>
                 <xsl:if test="coam/content/pageSpecific/errorMessage/@type = 'wrongPassword'">Passordet var feil
        </xsl:if>
           		 <xsl:if test="coam/content/pageSpecific/errorMessage/@type = 'noSuchUser'">Brukeren du prøvde å logge på med finnes ikke. Sjekk om du stavet det riktig
        </xsl:if>
                 <xsl:if test="coam/content/pageSpecific/errorMessage/@type = 'notConfirmed'">Brukeren du prøde å logge på har ikke blitt bekreftet ennå. Dette må gjøres av en administrator.
        </xsl:if>
        
        <!-- EditUser can give us there errorMessages:
            type= noPasswordProvided
            type=wrongPassword
            type=noSuchUser
            type=userNotSpecified
            type=noSuchUser
            type=notAuthorized
            type=passwordNotRepeated
            type=repeatPassTypo
            type=invalidInput
            type=noChanges-->
            
                 <xsl:if test="coam/content/pageSpecific/errorMessage/@type = 'passwordNotRepeated'">Passordet måtte skrives inn to ganger!
        </xsl:if>
                         <xsl:if test="coam/content/pageSpecific/errorMessage/@type = 'repeatPassTypo'">Du skrev ikke samme strengen i begge feltene hvor du skulle gjenta passordet!
        </xsl:if>           
                 <xsl:if test="coam/content/pageSpecific/errorMessage/@type = 'invalidInput'">Det var noe feil med det du skrev inn. I denne versjonen av COAM opplyser ikke systemet om hva som var feil. Det er enten for lange ord eller feil i e-postadressen.
        </xsl:if>
                 <xsl:if test="coam/content/pageSpecific/errorMessage/@type = 'noChanges'">Du gjorde ingen endringer i skjemaet
        </xsl:if>   
                         <xsl:if test="coam/content/pageSpecific/errorMessage/@type = 'notLoggedIn'">Du er ikke logget inn!!
        </xsl:if>   
        
        <!-- UploadPicture can give us: pictureUploadFailed --> 
                         <xsl:if test="coam/content/pageSpecific/errorMessage/@type = 'pictureUploadFailed'">Bildeopplasting feilet, i denne versjonen av COAM så opplyses desverre ikke grunnen til dette. Prøv å endre filnavnet, for det kan være at det finnes et bilde med samme filnavn fra før. Det skal ikke være sånn selvsagt, men denne begrensningen ligger i databasen til COAM. Det skal endres i senere versjoner.
        </xsl:if>    
        
        <!-- NewUser can give us errorMessage requiredFieldsMissing, mailAlreadyTaken and usernameTaken-->     
                 <xsl:if test="coam/content/pageSpecific/errorMessage/@type = 'requiredFieldsMissing'">Du fylte ikke inn alle felter
        </xsl:if>                
                 <xsl:if test="coam/content/pageSpecific/errorMessage/@type = 'mailAlreadyTaken'">E-postadressen du oppgav er allerede registrert på en annen bruker.
        </xsl:if>
                         <xsl:if test="coam/content/pageSpecific/errorMessage/@type = 'usernameTaken'">Brukernavnet du oppgav er allerede brukt. Prøv et annet et.
        </xsl:if>            
          
    <!-- End of errorMessage-->
    <!-- Then, if there are infoMessages, insert them.--> 
    <xsl:if test="coam/content/pageSpecific/infoMessage">
        <img src="design/basic/images/success.jpg" alt="Handling utført" width="250" height="178" />
        <br/>
    	<!-- ShowCategory can give us:
        type= newCategoryAdded
        additionalInfo= [the name of the new category]-->
       <xsl:if test="coam/content/pageSpecific/infoMessage/@type = 'newCategoryAdded'">Ny kategori lagt til med navnet: <a><xsl:attribute name="href">?module=ShowCategory&amp;title=<xsl:value-of select="coam/content/pageSpecific/infoMessage/@additionalInfo" /></xsl:attribute> <xsl:value-of select="coam/content/pageSpecific/infoMessage/@additionalInfo" /></a></xsl:if>
       
       <!-- Login can give us this infoMessage:
       type=userLoggedOn-->
                 <xsl:if test="coam/content/pageSpecific/infoMessage/@type = 'userLoggedOn'">Du er nå logget inn!
        </xsl:if>       
</xsl:if>

		<!-- EditUser can give us infoMessage userEdited -->
          <xsl:if test="coam/content/pageSpecific/infoMessage/@type = 'userEdited'">Endringene du registrerte på brukeren er nå lagret.
        </xsl:if>   
        
        <!-- ConfigureCOAM can give us infoMessage configChanged-->
        
                  <xsl:if test="coam/content/pageSpecific/infoMessage/@type = 'configChanged'">Konfigurasjonen av COAM-CMS er endret. Merk at du må oppdatere siden eller navigere til en annen side for å se endringene, fordi dataene er endret etter at de ble lagt inn i denne visningen! En svakhet som vil bli rettet opp i senere versjoner.
        </xsl:if> 
        
        <!-- EditPost can give us postDeleted and postUpdated-->
          <xsl:if test="coam/content/pageSpecific/infoMessage/@type = 'postDeleted'">Posten er nå slettet
        </xsl:if>         
          <xsl:if test="coam/content/pageSpecific/infoMessage/@type = 'userEdited'">Posten er nå oppdatert
        </xsl:if>     
          <xsl:if test="coam/content/pageSpecific/infoMessage/@type = 'postSaved'">Posten er lagret.
        </xsl:if>        
            
        
<!-- End of infoMessage-->
    <!-- Then, we insert the content of the module.-->
    
        <xsl:if test="coam/content/pageSpecific/module/Index">
        	<xsl:choose>
        	<xsl:when test="coam/content/pageSpecific/module/Index/iCategory">
        <xsl:for-each select="coam/content/pageSpecific/module/Index/iCategory">
        <h2><a><xsl:attribute name="href">?module=ShowCategory&amp;category=<xsl:value-of select="@name" /></xsl:attribute>
<xsl:value-of select="@name"/> :</a></h2>

<xsl:if test="not(iPost)">
Denne kategorien har ikke noen poster ennå
</xsl:if>
			
            <xsl:for-each select="iPost">
            <table width="75%" border="1" cellspacing="2">
  <tr>
    <td>
      <xsl:choose>
    <xsl:when test="@picture">
    <!-- The image tag is not a properly empty tag here: it has an opening tag and a close tag. I don't think this is valid, but I can't see how I should be able to set the attributes of the img tag otherwise-->
    <img> <xsl:attribute name="src">pictures/<xsl:value-of select="@author" />/smallThumbs/<xsl:value-of select="@picture" /></xsl:attribute> <xsl:attribute name="alt">Bilde valgt for innlegget &quot;<xsl:value-of select="@title" />&quot;</xsl:attribute> </img>
    </xsl:when>
    <xsl:otherwise>
    <img src="design/basic/images/defaultPostImage.jpg" alt="Ingen bilde er tilgjengelig" />
    </xsl:otherwise>
    </xsl:choose></td>
    <td rowspan="3" valign="middle"><xsl:value-of select="." /> <a><xsl:attribute name="href">?module=ReadPost&amp;id=<xsl:value-of select="@id" /></xsl:attribute>
Les mer</a> </td>
  </tr>
  <tr>
    <td class="subTitleStyle"><xsl:value-of select="@title" />    </td>
  </tr>
  <tr>
    <td>
    Forfatter: <a><xsl:attribute name="href">?module=UserInfo&amp;username=<xsl:value-of select="@author" /></xsl:attribute><xsl:value-of select="@author" /></a>    </td>
  </tr>
  <tr>
    <td>Laget: <xsl:value-of select="@createdAt" />    </td>
    <td valign="bottom">
    
    <xsl:if test="@editedAt">
    	Sist endret: <xsl:value-of select="@editedAt" /> av bruker <a><xsl:attribute name="href">?module=UserInfo&amp;username=<xsl:value-of select="@editedBy" /></xsl:attribute><xsl:value-of select="@editedBy" /></a>
    </xsl:if>    <xsl:if test="coam/userLoggedOn/@role = 'ADMIN' or coam/userLoggedOn/@username = @author">
    		<a><xsl:attribute name="href">@module=EditPost&amp;id=<xsl:value-of select="@id" /></xsl:attribute>redigèr</a>
    </xsl:if></td>
  </tr>
</table>
            </xsl:for-each>
        </xsl:for-each>
        </xsl:when>
        <xsl:otherwise>
        Det finnes ikke noen kategorier. Administratorer kan opprette kategorier. 
        	<xsl:if test="coam/userLoggedOn/@role = 'ADMIN'">
            <a><xsl:attribute name="href">?ShowCategory?action=makeCategorySchema</xsl:attribute>Opprett en kategori</a>
            </xsl:if>
        </xsl:otherwise>
        </xsl:choose>
        </xsl:if>
        
        <xsl:if test="coam/content/pageSpecific/module/ReadPost/post">
    <h2 align="center"> i kategorien <a> <xsl:attribute name="href">?module=ShowCategory&amp;category=<xsl:value-of select="coam/content/pageSpecific/module/ReadPost/post/@category" /></xsl:attribute><xsl:value-of select="coam/content/pageSpecific/module/ReadPost/post/@category"/></a>, skrevet av <a> <xsl:attribute name="href">?module=UserInfo&amp;username=<xsl:value-of select="coam/content/pageSpecific/module/ReadPost/post/@author" /></xsl:attribute>
    <xsl:value-of select="coam/content/pageSpecific/module/ReadPost/post/@author"/></a>&nbsp;<xsl:value-of select="coam/content/pageSpecific/module/ReadPost/post/@createdAt"/></h2>
    <p>
    <br/>
    <xsl:if test="coam/content/pageSpecific/module/ReadPost/post/@picture"><div><a> <xsl:attribute name="href">?module=ShowPicture&amp;filename=<xsl:value-of select="coam/content/pageSpecific/module/ReadPost/post/@picture"/></xsl:attribute><img border="0"> <xsl:attribute name="src">pictures/<xsl:value-of select="coam/content/pageSpecific/module/ReadPost/post/@author" />/<xsl:value-of select="coam/content/pageSpecific/module/ReadPost/post/@picture" /></xsl:attribute> <xsl:attribute name="alt">Bilde valgt for innlegget &quot;<xsl:value-of select="coam/content/pageSpecific/module/ReadPost/post/@title" />&quot;</xsl:attribute></img></a></div></xsl:if>
    
    <xsl:if test="coam/userLoggedOn/@role ='ADMIN' or coam/userLoggedOn/@userName = coam/content/pageSpecific/module/ReadPost/post/@author"><a> <xsl:attribute name="href">?module=EditPost&amp;id=<xsl:value-of select="coam/content/pageSpecific/module/ReadPost/post/@id"/></xsl:attribute>Klikk her for å redigere posten</a></xsl:if>
    <br/>
    <br/>
        <xsl:value-of select="coam/content/pageSpecific/module/ReadPost/post"/>
        <br/>
        <br/>
        <xsl:if test="coam/content/pageSpecific/module/ReadPost/post/@editedAt">
          redigert <xsl:value-of select="coam/content/pageSpecific/module/ReadPost/post/@editedAt"/> av <a> 
            <xsl:attribute name="href">?module=UserInfo&amp;username=<xsl:value-of select="coam/content/pageSpecific/module/ReadPost/post/@author" /></xsl:attribute>
            <xsl:value-of select="coam/content/pageSpecific/module/ReadPost/post/@editedBy"/></a>
        </xsl:if>
    </p>
    <p><br/>
    <xsl:if test="coam/content/pageSpecific/module/ReadPost/picture">
    Bilder: Klikk for å forstørre og få informasjon:
    <br/>
    <xsl:for-each select="coam/content/pageSpecific/module/ReadPost/picture">
    <a> <xsl:attribute name="href">?module=ShowPicture&amp;filename=<xsl:value-of select="@fileName"/></xsl:attribute><img class="thumbGallery"> 
    <xsl:attribute name="src">pictures/<xsl:value-of select="@uploadedBy" />/thumbs/<xsl:value-of select="@fileName" /></xsl:attribute> <xsl:attribute name="alt"><xsl:value-of select="@description" /></xsl:attribute> </img></a>
    </xsl:for-each>
    </xsl:if>
    </p>
    </xsl:if>
    <!-- End of content for ReadPost-->

	<!-- Start of content for UserInfo-->
    <xsl:if test="coam/content/pageSpecific/module/UserInfo/user">
    	<div>
        	<xsl:choose>
        		<xsl:when test="coam/content/pageSpecific/module/UserInfo/user/@pictureFile">
            		<img> <xsl:attribute name="src">pictures/<xsl:value-of select="coam/content/pageSpecific/module/UserInfo/user/@userName" />/thumbs/<xsl:value-of select="coam/content/pageSpecific/module/UserInfo/user/@pictureFile"/></xsl:attribute> /</img>
       	    	</xsl:when>
      	      	<xsl:otherwise>
                <img src="design/basic/images/defaultPostImage2.jpg" alt="denne brukeren har ikke noe bilde tilknyttet sin profil" />
      	      </xsl:otherwise>
    	    </xsl:choose>
        </div>
        <br/>
		Rolle: 
        <xsl:if test="coam/content/pageSpecific/module/UserInfo/user/@role = 'ADMIN'">
        Administrator
        </xsl:if>
            <xsl:if test="coam/content/pageSpecific/module/UserInfo/user/@role = 'USER'">
        Vanlig bruker
        </xsl:if>
        <br/>
        Fullt Navn: <xsl:value-of select="coam/content/pageSpecific/module/UserInfo/user/@firstName" /> <xsl:value-of select="coam/content/pageSpecific/module/UserInfo/user/@lastName" />
        <br/>
        Epost: Vises ikke grunnet frykt for spam, phishing, nigeriascammere og andre trusler!
        <br/>
        <xsl:if test="coam/content/pageSpecific/module/UserInfo/user/@isConfirmed = 'false'">
        Denne brukeren er ikke bekreftet ennå! <xsl:if test="coam/userLoggedOn/@role = 'ADMIN'"><a><xsl:attribute name="href">?module=EditUser&amp;username=<xsl:value-of select="coam/content/pageSpecific/module/UserInfo/user/@userName" />&amp;action=confirm</xsl:attribute>Klikk her for å bekrefte brukeren...</a></xsl:if> <br/>
        </xsl:if>
        Brukeren ble laget: <xsl:value-of select="coam/content/pageSpecific/module/UserInfo/user/@createdAt" />
    
    <br/>
    <xsl:if test="coam/content/pageSpecific/module/UserInfo/user/@userName = coam/userLoggedOn/@userName">
      <span class="red">DETTE ER DEG!</span><br/>
    </xsl:if>
    <xsl:choose>    <xsl:when test="coam/userLoggedOn/@role = 'ADMIN' and coam/content/pageSpecific/module/UserInfo/user"><a><xsl:attribute name="href">?module=EditUser&amp;username=<xsl:value-of select="coam/content/pageSpecific/module/UserInfo/user/@userName" /></xsl:attribute>Klikk her for å redigere profil...</a></xsl:when>
    <xsl:otherwise>
    <xsl:if test="coam/content/pageSpecific/module/UserInfo/user/@userName = coam/userLoggedOn/@userName"><a><xsl:attribute name="href">?module=EditUser&amp;username=<xsl:value-of select="coam/content/pageSpecific/module/UserInfo/user/@userName" /></xsl:attribute>Klikk her for å redigere profil...</a></xsl:if>
    </xsl:otherwise>
    </xsl:choose>
    
    </xsl:if>
    
    <!-- End of content for UserInfo-->
    
    <!-- Start of content for ShowCategories -->
    <xsl:if test="coam/content/pageSpecific/module/ShowCategories">
    	<xsl:for-each select="coam/content/menu/menuCategory">
          <a><xsl:attribute name="href">?module=ShowCategory&amp;category=<xsl:value-of select="@name" /></xsl:attribute>
<xsl:value-of select="@name"/> (<xsl:value-of select="@numberOfPosts"/>) - Beskrivelse: <xsl:value-of select="@description"/></a><br/>
            </xsl:for-each>
                  <br/>
           	<xsl:if test="coam/userLoggedOn/@role = 'ADMIN'">
            <a><xsl:attribute name="href">?ShowCategory?action=makeCategorySchema</xsl:attribute>Opprett en kategori</a>
            </xsl:if>   
    </xsl:if>
    <!-- End of content for ShowCategories -->
    
    <!-- Start of content for ShowCategory -->
    <xsl:if test="coam/content/pageSpecific/module/ShowCategory">
    	<xsl:if test="coam/content/pageSpecific/module/ShowCategory/@action = 'makeCategorySchema'">
        <!-- Content of the schema that creates a category -->
            <form id="form3" method="get" action="?module=ShowCategory">
            	<div><h2>Opprette ny kategori:</h2>
            	  <p>Navn på kategori:
            	    <input name="makeCategory" type="text" id="makeCategory" maxlength="25" />
            	     *Påkrevd<br />
            	    Beskrivelse av kategori:<br />
            	    <textarea name="makeDescription" id="makeDescription" cols="45" rows="5"></textarea>
            	    <br />
            	  *Påkrevd</p>
            	  <p>
            	    <input type="submit" name="module" id="button" value="ShowCategory" />
            	  </p>

            	</div>
        	</form>
            <!-- End: Content of the schema that creates a category -->
        </xsl:if>
			<!-- If the ShowCategory tag has a title attribute, we show that category-->
            <xsl:if test="coam/content/pageSpecific/module/ShowCategory/@title">
            	<div>
                    <h2>Beskrivelse: <xsl:value-of select="coam/content/pageSpecific/module/ShowCategory/@description"/></h2>
                    <!-- If there are any posts in this category, show the the same way as they are shown on the front page 
                    	 Else: Tell the user there are no posts in this category yet.-->
                  <xsl:choose>
                   	<xsl:when test="coam/content/pageSpecific/module/ShowCategory/iPost">
                         	<!-- We are here because there are posts. Show them.-->
                   	  <xsl:for-each select="coam/content/pageSpecific/module/ShowCategory/iPost">
            					<table width="75%" border="1" cellspacing="2">
  									<tr>
    									<td>
     									 <xsl:choose>
										   <xsl:when test="@picture">
    <!-- The image tag is not a properly empty tag here: it has an opening tag and a close tag. I don't think this is valid, but I can't see how I should be able to set the attributes of the img tag otherwise-->
    										<img> <xsl:attribute name="src">pictures/<xsl:value-of select="@author" />/smallThumbs/<xsl:value-of select="@picture" /></xsl:attribute> <xsl:attribute name="alt">Bilde valgt for innlegget &quot;<xsl:value-of select="@title" />&quot;</xsl:attribute> </img>
   										   </xsl:when>
   										 <xsl:otherwise>
  											  <img src="design/basic/images/defaultPostImage.jpg" alt="Ingen bilde er tilgjengelig" />
                                          </xsl:otherwise>
                                          </xsl:choose></td>
    								<td rowspan="3" valign="middle"><xsl:value-of select="." /> <a><xsl:attribute name="href">?module=ReadPost&amp;id=<xsl:value-of select="@id" /></xsl:attribute>
Les mer</a> </td>
  									</tr>
  									<tr>
   								 <td class="subTitleStyle"><xsl:value-of select="@title" />    </td>
  								</tr>
  								<tr>
  									  <td>
 									   Forfatter: <a><xsl:attribute name="href">?module=UserInfo&amp;username=<xsl:value-of select="@author" /></xsl:attribute><xsl:value-of select="@author" /></a>  									  </td>
 								 </tr>
								  <tr>
   								 <td>Laget: <xsl:value-of select="@createdAt" /> 								   </td>
 								   <td valign="bottom">
    
 								   <xsl:if test="@editedAt">
   								 	Sist endret: <xsl:value-of select="@editedAt" /> av bruker <a><xsl:attribute name="href">?module=UserInfo&amp;username=<xsl:value-of select="@editedBy" /></xsl:attribute><xsl:value-of select="@editedBy" /></a>
   								 </xsl:if>   								 <xsl:if test="coam/userLoggedOn/@role = 'ADMIN' or coam/userLoggedOn/@username = @author">
    		<a><xsl:attribute name="href">@module=EditPost&amp;id=<xsl:value-of select="@id" /></xsl:attribute>redigèr</a>
    </xsl:if></td>
  							</tr>
						</table>
          			  </xsl:for-each>
                    </xsl:when>
                    <xsl:otherwise>
                            <!-- There were no posts - tell the user this-->
                            Denne kategorien har ikke noen poster ennå. 
                            <xsl:if test="coam/userLoggedOn"><a><xsl:attribute name="href">?module=WritePost&amp;category=<xsl:value-of select="coam/content/pageSpecific/module/ShowCategory/@title" /></xsl:attribute>Vær den første til å skrive en post i denne kategorien</a></xsl:if>
                    </xsl:otherwise>
                  </xsl:choose>
                    <!-- If the user is logged in, he should be able to write a new post in this category, so provide a link -->
                </div>
            </xsl:if>
    </xsl:if>
    <!-- End of content for ShowCategory -->
    <!-- Start of content for Login -->
    <xsl:if test="coam/content/pageSpecific/module/Login/@showSchema = 'true'">
      For å logge inn benytt skjema til høyre...
    </xsl:if>
    <!-- End of content for Login -->
    <!-- Start of content for EditUser -->
    <xsl:if test="coam/content/pageSpecific/module/EditUser/user">
    <form id="form4" method="post">
    <xsl:attribute name="action">?module=EditUser&amp;username=<xsl:value-of select="coam/content/pageSpecific/module/EditUser/user/@userName" /></xsl:attribute>
      <p>Fornavn:
        <input name="setFirstName" type="text" id="setFirstName" maxlength="30">
        <xsl:attribute name="value"><xsl:value-of select="coam/content/pageSpecific/module/EditUser/user/@firstName" /></xsl:attribute>
        </input>
          <br />  
        <br />
        Etternavn:
        <input name="setLastName" type="text" id="setLastName" maxlength="30">
        <xsl:attribute name="value"><xsl:value-of select="coam/content/pageSpecific/module/EditUser/user/@lastName" /></xsl:attribute>        </input>
        
        <br />
        Bildefil:
        <input name="setPictureFile" type="text" id="setPictureFile" maxlength="30">
        <xsl:attribute name="value"><xsl:value-of select="coam/content/pageSpecific/module/EditUser/user/@pictureFile" /></xsl:attribute>
        </input>
        
        *<br />
        E-Post:
        <input name="setEmail" type="text" id="setEmail" maxlength="30">
                <xsl:attribute name="value"><xsl:value-of select="coam/content/pageSpecific/module/EditUser/user/@eMail" /></xsl:attribute>
        </input>                
      </p>
      <p>* - Bildefilen du skriver inn må du ha lastet opp og det må være riktig stavet! Denne tidlige versjonen av COAM sjekker ikke om det stemmer, men det finner du ut ved å gå inn på profilen din etterpå og se om bildet er der.</p>
      <p>Du kan endre passordet ditt her. Om du ikke vil endre det, la feltene stå tomme</p>
      <p>Ditt nåværende passord:
        <input name="oldPassword" type="password" id="oldPassword" maxlength="30" />
        <br />
      Det nye passordet:
      <input name="setPassword" type="password" id="setPassword" maxlength="30" />
      <br />
      <br />
      Gjenta det nye passordet:
      <input name="setPasswordRepeat" type="password" id="setPasswordRepeat" maxlength="30" />
      </p>
      <p>
        <input type="submit" name="submit" id="submit" value="Lagre endringer" />
      </p>
    </form>
    </xsl:if>
    <!-- End of content for EditUser -->
    
    <!-- Start of content for WritePost-->
    <xsl:if test="coam/content/pageSpecific/module/WritePost">
      <div>
        <form id="form5" method="post" action="?module=WritePost">
          Tittel:
          <input name="title" type="text" id="title" maxlength="50" />
        *
        <p>
          <label>
            <input type="radio" name="status" value="POSTD" id="Status_0" />
            Publisert</label>
          <br />
          <label>
            <input name="status" type="radio" id="Status_1" value="DRAFT" checked="checked" />
            Kladd</label>
          </p>
        <p>Innhold:<br />
          <textarea name="content" id="content" cols="45" rows="5"></textarea>          
          *</p>
          <xsl:choose>
          <xsl:when test="coam/content/menu/menuCategory">
        <p>Kategori:<br />
          <select name="category" id="category">
          <xsl:for-each select="coam/content/menu/menuCategory">
            <option><xsl:attribute name="value">
          		<!-- if we want the option to be selected, we must break the rules of xhtml. Therefore, this is not done
                in this version. This issue will hopefully be solved in later iterations-->
              <xsl:value-of select="@name" />
            </xsl:attribute>
            <xsl:value-of select="@name" />            </option>
            </xsl:for-each>
                    </select>
				</p>
			</xsl:when>
            <xsl:otherwise>
              <span class="style1">            Det finnes ikke noe kategori ennå, så du kan ikke skrive noe post. Er du administrator, kan du opprette en via administratorpanelet. Hvis ikke, ta kontakt med en administrator. Selvfølgelig leit at du får feilmeldingen her, men det er det ikke noe å gjøre med. Dette er den første versjonen av COAM-CMS  og dette er kodet nær innleveringsfristen!!!           </span>
            </xsl:otherwise>
            </xsl:choose>
        <p>Hvis du vil at posten skal ha et av bildene du har lastet opp som valgt bilde, skriv inn filnavnet i denne boksen:<br /> 
          <input type="text" name="picture" id="picture" />
          </p>
        <p>* - påkrevde felter</p>
        <p>
          <input type="submit" name="lagre" id="lagre" value="Lagre" />
          <br />
        </p>
        <br />
        </form>
      </div>
    </xsl:if>
    <!-- End of content for WritePost-->
    
    <!-- Start of content for UploadPicture-->
    <xsl:if test="coam/content/pageSpecific/module/UploadPicture">
      <form id="form6" enctype="multipart/form-data" method="post" action="?module=UploadPicture">
        <p>
          <input type="hidden" name="MAX_FILE_SIZE" value="5000000" />
          
          Filbane:
          <input type="file" name="picture" id="pictureFile" />
          <br />
        Navn: 
        <input name="name" type="text" id="name" maxlength="50" />
        <br />
        KORT beskrivelse:
        <input name="description" type="text" id="description2" maxlength="50" />
        </p>
        <p>
          <input type="submit" name="button5" id="button5" value="Last opp" />
        </p>
        <p>Maks størrelse er 5 megabyte! Og det bør holde!  </p>
      </form>
    </xsl:if>
    <!-- End of content for UploadPicture-->
    
    <!-- Start of content for AdministratorMenu-->
    <xsl:if test="coam/content/pageSpecific/module/AdministratorMenu">
        <xsl:if test="coam/userLoggedOn/@role = 'ADMIN'">
          <p><a href="?module=ConfigureCOAM">Konfigurere COAM</a></p>
          <p><a href="?module=AdministrateUsers">Administrere brukere</a></p>
          <p><a href="?module=NewUser">Ny bruker</a> (som administrator kan du lage administratorbrukere)</p>
          <p><a href="?module=ShowCategory&amp;action=makeCategorySchema">Ny kategori</a></p>
          <p>(resten kan du gjøre ved å bare klikke deg rundt)          </p>
        </xsl:if>
    </xsl:if>
    <!-- End of content for AdministratorMenu-->
    
    <!-- Start of content for ConfigureCOAM-->
    <xsl:if test="coam/content/pageSpecific/module/ConfigureCOAM">
    	<xsl:if test="coam/userLoggedOn/@role = 'ADMIN'">
    	  <form id="form7" method="post" action="?module=ConfigureCOAM">
    	    <p>Tittel:
    	      <input type="text" name="title" id="title"><xsl:attribute name="value"><xsl:value-of select="coam/title" /></xsl:attribute></input>
    	        <br />
    	      Undertittel:
    	      <input type="text" name="sub" id="sub"><xsl:attribute name="value"><xsl:value-of select="coam/subtitle" /></xsl:attribute></input>
    	      <br />
    	      Beskrivelse:
    	      <textarea name="description" id="description" cols="45" rows="5"><xsl:value-of select="coam/description" /></textarea>
    	      <br />
    	    Kontaktinfo: 
    	    <input type="text" name="contact" id="contact"><xsl:attribute name="value"><xsl:value-of select="coam/contact" /></xsl:attribute></input>
    	    <br />
    	    Antall tegn i sammendrag av poster:
    	    <input type="text" name="summary" id="summary"><xsl:attribute name="value"><xsl:value-of select="coam/content/pageSpecific/module/ConfigureCOAM/@summaryLength" /></xsl:attribute></input>
</p>
    	    <p>
    	      <input type="submit" name="button2" id="button2" value="Lagre endringer" />
    	    </p>
    	  </form>
    	</xsl:if>
    </xsl:if>
    <!-- End of content for ConfigureCOAM-->
    
    <!-- Start of content for EditPicture (which, sadly is not implemented yet)-->
    
    <xsl:if test="coam/content/pageSpecific/module/EditPicture">
    Desverre, det ble ikke tid til å implementere mulighet for å redigere bilder. Skal du slette bilder eller endre noe informasjon om bildet, ta kontakt med en administrator, så får de sende klagebrev til studenten som lagde COAM.
    </xsl:if>    
    
    <!-- End of content for EditPicture-->
    
    <!-- Start of content for EditPost -->
    <xsl:if test="coam/content/pageSpecific/module/EditPost">
    	<xsl:if test="coam/content/pageSpecific/module/EditPost/@showSchema = 'true'">
    	  <form id="form8" method="post"><xsl:attribute name="action">?module=EditPost&amp;id=<xsl:value-of select="coam/content/pageSpecific/module/EditPost/post/@id" /></xsl:attribute>
    	    <p>Tittel:
    	      <input type="text" name="title" id="title">
    	      <xsl:attribute name="value"><xsl:value-of select="coam/content/pageSpecific/module/EditPost/post/@title" /></xsl:attribute>
    	      </input>
    	        <br />
    	      Bildefil:
    	      <input type="text" name="picture" id="picture2">
              <xsl:attribute name="value"><xsl:value-of select="coam/content/pageSpecific/module/EditPost/post/@picture" /></xsl:attribute>
              </input>
    	      Må være filnavnet på en fil du har lastet opp<br />
    	      Kategori:<br />
    	      <select name="category" id="category">
    	        <xsl:for-each select="coam/content/menu/menuCategory">
    	          <option>
    	            <xsl:attribute name="value">
    	              <!-- if we want the option to be selected, we must break the rules of xhtml. Therefore, this is not done
                in this version. This issue will hopefully be solved in later iterations-->
    	              <xsl:value-of select="@name" />                       </xsl:attribute>
    	              <xsl:value-of select="@name" />            </option>
  	          </xsl:for-each>
  	        </select>
    	      <br />
    	      Innhold: <br />
    	      <textarea name="content" id="content2" cols="45" rows="5"><xsl:value-of select="coam/content/pageSpecific/module/EditPost/post" />
             </textarea>
    	      </p>
    	    <p>
    	      <label>
    	      <input name="status" type="radio" id="status_0" value="POSTD"><xsl:if test="coam/content/pageSpecific/module/EditPost/post/@status = 'POSTD'"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if></input>
Postet</label>
    	      <br />
    	      <label>
    	      <input name="status" type="radio" id="status_1" value="DRAFT"><xsl:if test="coam/content/pageSpecific/module/EditPost/post/@status = 'DRAFT'"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
    	      </input>
Kladd</label>
    	      <br />
    	    </p>
    	    <p>
    	      <input type="submit" name="button3" id="button3" value="Lagre endringer" />
    	      <br />
    	      <br />
  	          </p>
    	  </form>
    	</xsl:if>
    </xsl:if>
     
    <!-- End of content for EditPost -->
    
    <!-- Start of content for MyPictures -->
    <xsl:if test="coam/content/pageSpecific/module/MyPictures">
    	<xsl:for-each select="coam/content/pageSpecific/module/MyPictures/picture">
        	<a>
            <xsl:attribute name="href">
            ?module=ShowPicture&amp;filename=<xsl:value-of select="@fileName" /></xsl:attribute>
            <img>
            <xsl:attribute name="src">pictures/<xsl:value-of select="@uploadedBy" />/thumbs/<xsl:value-of select="@fileName" />            </xsl:attribute></img></a>
        </xsl:for-each>
    </xsl:if>    
    
    <!-- End of content for MyPictures-->
 
    <!-- Start of content for NewUser -->
    <xsl:if test="coam/content/pageSpecific/module/NewUser">
      <form id="form9" method="post" action="?module=NewUser">
        <p>Ønsket brukernavn:
          <input type="text" name="username" id="username" />
          <br />
          Fornavn:
          <input type="text" name="firstname" id="firstname" />
          <br />
          Etternavn:
          <input type="text" name="lastname" id="lastname" />
          <br />
          E-Postadresse: 
          <input type="text" name="email" id="email" /> 
          Merk at det kun kan legges inn èn bruker per e-postadresse<br />
          Passord:
          <input type="password" name="password" id="password" />
          Ikke skriv feil!</p>
        <p>
        <xsl:if test="coam/userLoggedOn/@role = 'ADMIN'">
        </xsl:if>
        <label>
        <input name="role" type="radio" id="role_0" value="USER" checked="checked" />
Bruker</label>
        <br />
        <label>
        <input type="radio" name="role" value="ADMIN" id="role_1" />
Administrator</label>
        <br />
        <xsl:if test="coam/userLoggedOn/@role = 'ADMIN'">
        </xsl:if>
        <br />
          <input type="submit" name="button4" id="button4" value="Lag ny bruker..." />
          </p>
      </form>
    </xsl:if>
           
    
	<!-- End of content for NewUser -->
    
    <!-- Start of content for Search -->
    
    <xsl:if test="coam/content/pageSpecific/module/Search">
   		 <xsl:choose>
   			 <xsl:when test="coam/content/pageSpecific/module/Search/@searchString">
    		 Du søkte på: <xsl:value-of select="coam/content/pageSpecific/module/Search/@searchString" />
             <br/>
				<xsl:choose>
                	<xsl:when test="coam/content/pageSpecific/module/Search/iPost">
                    	Fant ordet du søkte på i følgende poster (stopper på 10 i denne versjonen): <br/>
                    	<xsl:for-each select="coam/content/pageSpecific/module/Search/iPost">
                        	            					<table width="75%" border="1" cellspacing="2">
  									<tr>
    									<td>
     									 <xsl:choose>
										   <xsl:when test="@picture">
    <!-- The image tag is not a properly empty tag here: it has an opening tag and a close tag. I don't think this is valid, but I can't see how I should be able to set the attributes of the img tag otherwise-->
    										<img> <xsl:attribute name="src">pictures/<xsl:value-of select="@author" />/smallThumbs/<xsl:value-of select="@picture" /></xsl:attribute> <xsl:attribute name="alt">Bilde valgt for innlegget &quot;<xsl:value-of select="@title" />&quot;</xsl:attribute> </img>
   										   </xsl:when>
   										 <xsl:otherwise>
  											  <img src="design/basic/images/defaultPostImage.jpg" alt="Ingen bilde er tilgjengelig" />
                                          </xsl:otherwise>
                                          </xsl:choose></td>
    								<td rowspan="3" valign="middle"><xsl:value-of select="." /> <a><xsl:attribute name="href">?module=ReadPost&amp;id=<xsl:value-of select="@id" /></xsl:attribute>
Les mer</a> </td>
  									</tr>
  									<tr>
   								 <td class="subTitleStyle"><xsl:value-of select="@title" />    </td>
  								</tr>
  								<tr>
  									  <td>
 									   Forfatter: <a><xsl:attribute name="href">?module=UserInfo&amp;username=<xsl:value-of select="@author" /></xsl:attribute><xsl:value-of select="@author" /></a>  									  </td>
 								 </tr>
								  <tr>
   								 <td>Laget: <xsl:value-of select="@createdAt" /> 								   </td>
 								   <td valign="bottom">
    
 								   <xsl:if test="@editedAt">
   								 	Sist endret: <xsl:value-of select="@editedAt" /> av bruker <a><xsl:attribute name="href">?module=UserInfo&amp;username=<xsl:value-of select="@editedBy" /></xsl:attribute><xsl:value-of select="@editedBy" /></a>
   								 </xsl:if>   								 <xsl:if test="coam/userLoggedOn/@role = 'ADMIN' or coam/userLoggedOn/@username = @author">
    		<a><xsl:attribute name="href">@module=EditPost&amp;id=<xsl:value-of select="@id" /></xsl:attribute>redigèr</a>
    </xsl:if></td>
  							</tr>
						</table>
                        </xsl:for-each>
                    </xsl:when>
                    <xsl:otherwise>
                    Søket returnerte ingen treff...
                    </xsl:otherwise>
                </xsl:choose>
    		 </xsl:when>
    		 <xsl:otherwise>
             Du kan søke til venstre der...
             </xsl:otherwise>
   		 </xsl:choose>
    
    </xsl:if>
    <!-- End of content for Search -->
    
    <!-- Start of content for MyPosts -->
    
	<xsl:if test="coam/content/pageSpecific/module/MyPosts">    
    	                    	<xsl:for-each select="coam/content/pageSpecific/module/MyPosts/iPost">
                        	            					<table width="75%" border="1" cellspacing="2">
  									<tr>
    									<td>
     									 <xsl:choose>
										   <xsl:when test="@picture">
    <!-- The image tag is not a properly empty tag here: it has an opening tag and a close tag. I don't think this is valid, but I can't see how I should be able to set the attributes of the img tag otherwise-->
    										<img> <xsl:attribute name="src">pictures/<xsl:value-of select="@author" />/smallThumbs/<xsl:value-of select="@picture" /></xsl:attribute> <xsl:attribute name="alt">Bilde valgt for innlegget &quot;<xsl:value-of select="@title" />&quot;</xsl:attribute> </img>
   										   </xsl:when>
   										 <xsl:otherwise>
  											  <img src="design/basic/images/defaultPostImage.jpg" alt="Ingen bilde er tilgjengelig" />
                                          </xsl:otherwise>
                                          </xsl:choose></td>
    								<td rowspan="3" valign="middle"><xsl:value-of select="." /> <a><xsl:attribute name="href">?module=ReadPost&amp;id=<xsl:value-of select="@id" /></xsl:attribute>
Les mer</a> </td>
  									</tr>
  									<tr>
   								 <td class="subTitleStyle"><xsl:value-of select="@title" />    </td>
  								</tr>
  								<tr>
  									  <td>
 									   Forfatter: <a><xsl:attribute name="href">?module=UserInfo&amp;username=<xsl:value-of select="@author" /></xsl:attribute><xsl:value-of select="@author" /></a>  									  </td>
 								 </tr>
								  <tr>
   								 <td>Laget: <xsl:value-of select="@createdAt" /> 								   </td>
 								   <td valign="bottom">
    
 								   <xsl:if test="@editedAt">
   								 	Sist endret: <xsl:value-of select="@editedAt" /> av bruker <a><xsl:attribute name="href">?module=UserInfo&amp;username=<xsl:value-of select="@editedBy" /></xsl:attribute><xsl:value-of select="@editedBy" /></a>
   								 </xsl:if>   								 <xsl:if test="coam/userLoggedOn/@role = 'ADMIN' or coam/userLoggedOn/@userName = @author">
    		<a><xsl:attribute name="href">@module=EditPost&amp;id=<xsl:value-of select="@id" /></xsl:attribute>redigèr</a>
    </xsl:if></td>
  							</tr>
						</table>
                        </xsl:for-each>
    </xsl:if>
    
    <!-- End of content for MyPosts-->
    
    <!-- Start of content for AdministrateUsers-->
    <xsl:if test="coam/content/pageSpecific/module/AdministrateUsers">
    <xsl:for-each select="coam/content/pageSpecific/module/AdministrateUsers/user">
        	<div>
        	<xsl:choose>
        		<xsl:when test="@pictureFile">
            		<img> <xsl:attribute name="src">pictures/<xsl:value-of select="@userName" />/thumbs/<xsl:value-of select="@pictureFile"/></xsl:attribute> /</img>
       	    	</xsl:when>
      	      	<xsl:otherwise>
                <img src="design/basic/images/defaultPostImage2.jpg" alt="denne brukeren har ikke noe bilde tilknyttet sin profil" />
      	      </xsl:otherwise>
    	    </xsl:choose>
        </div>
            <p>Brukernavn: <xsl:value-of select="@userName" /><br />
              Rolle: 
              <xsl:if test="@role = 'ADMIN'">
                Administrator
              </xsl:if>
              <xsl:if test="@role = 'USER'">
                Vanlig bruker
              </xsl:if>
                <br/>
              Fullt Navn: <xsl:value-of select="@firstName" /> <xsl:value-of select="@lastName" />
              <br/>
              Epost: Vises ikke grunnet frykt for spam, phishing, nigeriascammere og andre trusler!
              <br/>
              <xsl:if test="@isConfirmed = 'false'">
                Denne brukeren er ikke bekreftet ennå!
                                  <a>
                    <xsl:attribute name="href">?module=EditUser&amp;username=<xsl:value-of select="@userName" />&amp;action=confirm</xsl:attribute>
                    Klikk her for å bekrefte brukeren...</a>
                </xsl:if>
               

                 
                  <br/>
              
              Brukeren ble laget: <xsl:value-of select="@createdAt" />
              
                <br/>  
                    <a>
                      <xsl:attribute name="href">?module=EditUser&amp;username=<xsl:value-of select="@userName" /></xsl:attribute>
                    Klikk her for å redigere profil...</a>
                <br/>
                <br/>
              </p>
            </xsl:for-each>
    </xsl:if>
    <!-- End of content for AdministrateUsers-->
    
    <!-- Start of content for ShowPicture-->
    <xsl:if test="coam/content/pageSpecific/module/ShowPicture">
    Navn på bilde: <xsl:value-of select="coam/content/pageSpecific/module/ShowPicture/picture/@title" /><br />
    Lastet opp av: <xsl:value-of select="coam/content/pageSpecific/module/ShowPicture/picture/@uploadedBy" /><br />
    Sist endret: <xsl:value-of select="coam/content/pageSpecific/module/ShowPicture/picture/@lastChanged" /><br />
    Beskrivelse: <xsl:value-of select="coam/content/pageSpecific/module/ShowPicture/picture/@description" /><br />
    <img><xsl:attribute name="src">pictures/<xsl:value-of select="coam/content/pageSpecific/module/ShowPicture/picture/@uploadedBy" />/<xsl:value-of select="coam/content/pageSpecific/module/ShowPicture/picture/@fileName" /></xsl:attribute></img>
    </xsl:if>
    <!--end of content for ShowPicture-->
    
    <!-- End of content for pageSpecific -->        </td>
    <td valign="top" id="rightMenu" style="width:15%">
    
    <xsl:choose>
    <xsl:when test="coam/userLoggedOn">
            <p>Du er logget inn.</p>
        <p>Bruker: <a> <xsl:attribute name="href">?module=UserInfo&amp;username=<xsl:value-of select="coam/userLoggedOn/@userName" /></xsl:attribute><xsl:value-of select="coam/userLoggedOn/@userName" /> (<xsl:value-of select="coam/userLoggedOn/@role" />)</a></p>
        <p><a> <xsl:attribute name="href">?module=WritePost</xsl:attribute>Skriv en post</a><br />
          <a href="?module=UploadPicture">Last opp bilde</a><br />
          <a href="?module=MyPosts">Mine poster</a><br />
          <a href="?module=MyPictures">Mine bilder</a></p>
        <p><a> <xsl:attribute name="href">?module=AdministratorMenu</xsl:attribute>Administratormeny</a></p>
        <p><a href="?logoff=true">Logg av</a></p>
    </xsl:when>
<xsl:otherwise>
<form action="index.php?module=Login" method="post" id="form2">
      <p>
        Brukernavn:
        <br/>
          <input name="user" type="text" id="user" maxlength="25" />
          <br/>
        Passord:
        <br/>
        <input type="password" name="pass" id="pass" />
        <br/>
        <input type="submit" name="logOn" id="logOn" value="Logg in" />
      </p>
      <p>Ikke registrert? <a href="?module=NewUser">Registrer deg</a><br />
      Har du glemt passordet ditt? Ta kontakt, for det er ikke implementert noe mulighet for å få tilsendt et nygenerert et ennå!</p>
</form>
</xsl:otherwise>
</xsl:choose>
      <p><img src="design/basic/images/madeonamac.gif" alt="Made on a mac" width="88" height="31" /></p>
      <p>
      	<a href="http://validator.w3.org/check?uri=referer"><img src="http://www.w3.org/Icons/valid-xhtml10-blue" alt="Valid XHTML 1.0 Strict" width="88" height="31" border="0" />
      	</a>
      </p>
      </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td width="93">&nbsp;</td>
    <td width="353">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>

</xsl:template>
</xsl:stylesheet>