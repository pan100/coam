<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns="http://www.w3.org/1999/xhtml"
                version="1.0" >
<xsl:template match="coam/content/pageSpecific/module/Index">
<h3>Hovedside - siste nytt i kategoriene:</h3>

        	<xsl:choose>
        	<xsl:when test="/coam/content/pageSpecific/module/Index/iCategory">
        <xsl:for-each select="//iCategory">
        <h2><a><xsl:attribute name="href">?module=ShowCategory&amp;category=<xsl:value-of select="@name" /></xsl:attribute>
<xsl:value-of select="@name"/> :</a></h2>

<xsl:if test="not(iPost)">
Denne kategorien har ikke noen poster ennå
</xsl:if>
			
            <xsl:for-each select="iPost">
            <table width="75%" border="1" cellspacing="2">
  <tr>
    <td>
      <xsl:choose>
    <xsl:when test="@picture">
    <!-- The image tag is not a properly empty tag here: it has an opening tag and a close tag. I don't think this is valid, but I can't see how I should be able to set the attributes of the img tag otherwise-->
    <img> <xsl:attribute name="src">pictures/<xsl:value-of select="@author" />/smallThumbs/<xsl:value-of select="@picture" /></xsl:attribute> <xsl:attribute name="alt">Bilde valgt for innlegget &quot;<xsl:value-of select="@title" />&quot;</xsl:attribute> </img>
    </xsl:when>
    <xsl:otherwise>
    <font size="+6">X</font>
    </xsl:otherwise>
    </xsl:choose></td>
    <td rowspan="3" valign="middle"><xsl:value-of select="." /> <a><xsl:attribute name="href">?module=ReadPost&amp;id=<xsl:value-of select="@id" /></xsl:attribute>
Les mer</a> </td>
  </tr>
  <tr>
    <td class="subTitleStyle"><xsl:value-of select="@title" />    </td>
  </tr>
  <tr>
    <td>
    Forfatter: <a><xsl:attribute name="href">?module=UserInfo&amp;username=<xsl:value-of select="@author" /></xsl:attribute><xsl:value-of select="@author" /></a>    </td>
  </tr>
  <tr>
    <td>Laget: <xsl:value-of select="@createdAt" />    </td>
    <td valign="bottom">
    
    <xsl:if test="@editedAt">
    	Sist endret: <xsl:value-of select="@editedAt" /> av bruker <a><xsl:attribute name="href">?module=UserInfo&amp;username=<xsl:value-of select="@editedBy" /></xsl:attribute><xsl:value-of select="@editedBy" /></a>
    </xsl:if>    <xsl:if test="coam/userLoggedOn/@role = 'ADMIN' or coam/userLoggedOn/@username = @author">
    		<a><xsl:attribute name="href">@module=EditPost&amp;id=<xsl:value-of select="@id" /></xsl:attribute>redigèr</a>
    </xsl:if></td>
  </tr>
</table>
            </xsl:for-each>
        </xsl:for-each>
        </xsl:when>
        <xsl:otherwise>
        Det finnes ikke noen kategorier. Administratorer kan opprette kategorier. 
        	<xsl:if test="coam/userLoggedOn/@role = 'ADMIN'">
            <a><xsl:attribute name="href">?ShowCategory?action=makeCategorySchema</xsl:attribute>Opprett en kategori</a>
            </xsl:if>
        </xsl:otherwise>
        </xsl:choose>
</xsl:template>
</xsl:stylesheet>