<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns="http://www.w3.org/1999/xhtml"
                version="1.0" >
<xsl:template match="coam/content/pageSpecific/module/ReadPost">
<h3>Visning av post -  &quot;<xsl:value-of select="/coam/content/pageSpecific/module/ReadPost/post/@title"/>&quot;</h3>

    <strong align="center"> i kategorien <a> <xsl:attribute name="href">?module=ShowCategory&amp;category=<xsl:value-of select="/coam/content/pageSpecific/module/ReadPost/post/@category" /></xsl:attribute><xsl:value-of select="/coam/content/pageSpecific/module/ReadPost/post/@category"/></a>, skrevet av <a> <xsl:attribute name="href">?module=UserInfo&amp;username=<xsl:value-of select="/coam/content/pageSpecific/module/ReadPost/post/@author" /></xsl:attribute><xsl:value-of select="/coam/content/pageSpecific/module/ReadPost/post/@author"/></a><a>&nbsp;</a> <xsl:value-of select="/coam/content/pageSpecific/module/ReadPost/post/@createdAt"/></strong>
    <p>
    <br/>
    <xsl:if test="/coam/content/pageSpecific/module/ReadPost/post/@picture"><div><a> <xsl:attribute name="href">?module=ShowPicture&amp;filename=<xsl:value-of select="/coam/content/pageSpecific/module/ReadPost/post/@picture"/></xsl:attribute><img border="0"> <xsl:attribute name="src">pictures/<xsl:value-of select="/coam/content/pageSpecific/module/ReadPost/post/@author" />/<xsl:value-of select="/coam/content/pageSpecific/module/ReadPost/post/@picture" /></xsl:attribute> <xsl:attribute name="alt">Bilde valgt for innlegget &quot;<xsl:value-of select="/coam/content/pageSpecific/module/ReadPost/post/@title" />&quot;</xsl:attribute></img></a></div></xsl:if>
    
    <xsl:if test="/coam/userLoggedOn/@role ='ADMIN' or coam/userLoggedOn/@userName = /coam/content/pageSpecific/module/ReadPost/post/@author"><a> <xsl:attribute name="href">?module=EditPost&amp;id=<xsl:value-of select="/coam/content/pageSpecific/module/ReadPost/post/@id"/></xsl:attribute>Klikk her for å redigere posten</a></xsl:if>
    <br/>
    <br/>
        <xsl:value-of select="/coam/content/pageSpecific/module/ReadPost/post"/>
        <br/>
        <br/>
        <xsl:if test="/coam/content/pageSpecific/module/ReadPost/post/@editedAt">
          redigert <xsl:value-of select="/coam/content/pageSpecific/module/ReadPost/post/@editedAt"/> av <a> 
            <xsl:attribute name="href">?module=UserInfo&amp;username=<xsl:value-of select="/coam/content/pageSpecific/module/ReadPost/post/@author" /></xsl:attribute>
            <xsl:value-of select="/coam/content/pageSpecific/module/ReadPost/post/@editedBy"/></a>
        </xsl:if>
    </p>
    <p><br/>
    <xsl:if test="/coam/content/pageSpecific/module/ReadPost/picture">
    Bilder: Klikk for å forstørre og få informasjon:
    <br/>
    <xsl:for-each select="/coam/content/pageSpecific/module/ReadPost/picture">
    <a> <xsl:attribute name="href">?module=ShowPicture&amp;filename=<xsl:value-of select="@fileName"/></xsl:attribute><img class="thumbGallery"> 
    <xsl:attribute name="src">pictures/<xsl:value-of select="@uploadedBy" />/thumbs/<xsl:value-of select="@fileName" /></xsl:attribute> <xsl:attribute name="alt"><xsl:value-of select="@description" /></xsl:attribute> </img></a>
    </xsl:for-each>
    </xsl:if>
    </p>

</xsl:template>
</xsl:stylesheet>