<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns="http://www.w3.org/1999/xhtml"
                version="1.0" >
				<!-- Includesetninger til alle moduler - de har hver sin xslt med et eget template for å holde bedre oversikt -->
                <xsl:include href="index.xsl"/>
                <xsl:include href="readpost.xsl"/>

<xsl:output method="html"
            media-type="text/html"
            doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
            doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
            indent="yes"
            encoding="UTF-8" />

<xsl:template match="/">
<html>
<head>
<title><xsl:value-of select="coam/title"/></title>
</head>
<body>
	
    <!-- Overskrift - tittelen på CMSet-->
	<h1 align="center"><xsl:value-of select="coam/title"/></h1>
    <h2 align="center"><xsl:value-of select="coam/subtitle"/></h2>
    
    <!-- Søkeboks-->
    	<center>
		<form action="index.php" method="get" id="form1">
        	<input type="text" name="search" id="search" />
        	<input name="send" type="submit" value="Søk..." />
        	<input type="hidden" name="module" id="module" value="Search"/>
        </form>
        </center>
    <!-- Kategorier ramset opp som lenker -->
    	<center>| 
        <xsl:for-each select="coam/content/menu/menuCategory">
        	<a><xsl:attribute name="href">?module=ShowCategory&amp;category=<xsl:value-of select="@name" /></xsl:attribute>
				<xsl:value-of select="@name"/> (<xsl:value-of select="@numberOfPosts"/>)
            </a> | 
        </xsl:for-each>
        </center>
        
    <!-- en lenke til hovedsiden om vi ikke befinner oss på den -->
    <xsl:choose>
		<xsl:when test="coam/content/pageSpecific/module/Index">
		</xsl:when>
		<xsl:otherwise>
        <small>
			<a href="index.php">Tilbake til hovedside</a>
        </small>
		</xsl:otherwise>
	</xsl:choose>
    
    <!-- Her kommer de forskjellige modulene i content-->
    <xsl:apply-templates select="coam/content/pageSpecific/module/Index"/>
    <xsl:apply-templates select="coam/content/pageSpecific/module/ReadPost"/>
    
    <!-- Til slutt kommer innloggingstatus og mulighet for å logge inn + kontaktinfoen-->
    
    	<hr/>
        <p>Kontakt: <xsl:value-of select="coam/contact"/></p>
        <xsl:choose>
    		<xsl:when test="coam/userLoggedOn">
            <p>Du er logget inn.</p>
        	<p>Bruker: <a> <xsl:attribute name="href">?module=UserInfo&amp;username=<xsl:value-of select="coam/userLoggedOn/@userName" /></xsl:attribute><xsl:value-of select="coam/userLoggedOn/@userName" /> (<xsl:value-of select="coam/userLoggedOn/@role" />)</a></p>
        <p>| <a> <xsl:attribute name="href">?module=WritePost</xsl:attribute>Skriv en post</a> |
          <a href="?module=UploadPicture">Last opp bilde</a> |
          <a href="?module=MyPosts">Mine poster</a> |
          <a href="?module=MyPictures">Mine bilder</a> |
        <a> <xsl:attribute name="href">?module=AdministratorMenu</xsl:attribute>Administratormeny</a> |
        <a href="?logoff=true">Logg av</a></p>
			</xsl:when>
			<xsl:otherwise>
				<form action="index.php?module=Login" method="post" id="form2">
      				<p>
       				 Brukernavn:
        			<br/>
          			<input name="user" type="text" id="user" maxlength="25" />
          			<br/>
        			Passord:
        			<br/>
        			<input type="password" name="pass" id="pass" />
        			<br/>
       				 <input type="submit" name="logOn" id="logOn" value="Logg in" />
      				</p>
     				 <p>Ikke registrert? <a href="?module=NewUser">Registrer deg</a><br />
      				Har du glemt passordet ditt? Ta kontakt, for det er ikke implementert noe mulighet for å få tilsendt et nygenerert et ennå!</p>
				</form>
			</xsl:otherwise>
		</xsl:choose>
</body>
</html>
</xsl:template>
</xsl:stylesheet>