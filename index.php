<?php
header("Content-Type: text/html;charset=UTF-8");
error_reporting(E_ALL|E_STRICT);
session_start();
require_once 'view/CommonOutput.php';
require_once 'controller/ViewModuleController.php';
require_once 'controller/ConfigController.php';
//check to see if the user has selected to log off
if(isset($_GET['logoff'])) {
	unset($_SESSION['user']);
	unset($_SESSION['userRole']);
}



//the DOM for the output XML
$outputDOM = new DOMDocument();
$outputDOM->encoding = 'UTF-8';
//insert common XML into the DOM
$commonOutputter = new CommonOutput($outputDOM);
$outputDOM = $commonOutputter->getUpdatedDOM();

//get the correct module and insert the correct XML
$outputModule = ViewModuleController::getCorrectModule($outputDOM);
$outputDOM = $outputModule->getUpdatedDOM();

//and finally, we output everything:
$outputDOM->formatOutput=true;
if(isset($_GET['outputMode'])) {
	print $outputDOM->saveXML();
}
else {
	$xslt = new DOMDocument();
	$xslt->load('design/'. ConfigController::getInstance()->getDesign() . '/style.xsl');
	$xslt->encoding = 'UTF-8';
	
	$proc = new XSLTProcessor();
	$proc->importStylesheet($xslt);
	$domForOutput = $proc->transformToDoc($outputDOM);
	echo $domForOutput->saveXML();
}


?>