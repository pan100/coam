<?php
class Picture {
	private $fileName;
	private $title;
	private $uploadedBy;
	private $lastChanged;
	private $description;
	
	function __construct($fileName, $title, $uploadedBy, $lastChanged, $description) {
		$this->fileName = $fileName;
		$this->title = $title;
		$this->uploadedBy = $uploadedBy;
		$this->lastChanged = $lastChanged;
		$this->description = $description;
	}
	
	function getFileName() {
		return $this->fileName;
	}
	
	function setFileName($fileName) {
		$this->fileName = $fileName;
	}
	
	function getTitle() {
		return $this->title;
	}
	
	function setTitle($title) {
		$this->title = $title;
	}
	
	function getUploadedBy() {
		return $this->uploadedBy;
	}
	
	function setUploadedBy($uploadedBy) {
		$this->uploadedBy = $uploadedBy;
	}
	
	function getLastChanged() {
		return $this->lastChanged;
	}
	
	function setLastChanged($lastChanged) {
		$this->lastChanged = $lastChanged;
	}
	
	function getDescription() {
		return $this->description;
	}
	
	function setDescription($description) {
		$this->description = $description;
	}
}
?>