<?php
class Post {
	private $id; //type int
	private $user; //type string
	private $status; //type string
	private $title; //type string
	private $content; //type string
	private $createdAt; //an int, but really a timestamp
	private $editedAt; //also really a timestamp
	private $editedBy; //type string
	private $picture; //The main picture of the post (filename)
	private $category; //the category
	
	function __construct($id, $user, $status, $title, $content, $createdAt, $editedAt, $editedBy, $picture, $category) {
		$this->id = $id;
		$this->user = $user;
		$this->status = $status;
		$this->title = $title;
		$this->content = $content;
		$this->createdAt = $createdAt;
		$this->editedAt = $editedAt;
		$this->editedBy = $editedBy;
		$this->picture = $picture;
		$this->category = $category;
		
	}
	
	function getId() {
		return $this->id;
	}
	
	function setId($id) {
		$this->id = $id;
	}
	
	function getUser() {
		return $this->user;
	}
	
	function setUser($user) {
		$this->user = $user;
	}
	
	function getStatus() {
		return $this->status;
	}
	
	function setStatus($status) {
		$this->status = $status;
	}
	
	function getTitle() {
		return $this->title;
	}
	
	function setTitle($title) {
		$this->title = $title;
	}
	
	function getContent() {
		return $this->content;
	}
	
	function setContent($content) {
		$this->content = $content;
	}
	
	function getCreatedAt() {
		return $this->createdAt;
	}
	
	
	function getEditedAt() {
		return $this->editedAt;
	}
	function getEditedBy() {
		return $this->editedBy;
	}
	
	function setEditedBy($editedBy) {
		$this->editedBy = $editedBy;
	}
	
	function getPicture() {
		return $this->picture;
	}
	
	function setPicture($picture) {
		$this->picture = $picture;
	}
	
	function getCategory() {
		return $this->category;
	}
	
	function setCategory($category) {
		$this->category = $category;
	}
}
?>