<?php

/*
 * Class User - Represents a user of COAM-CMS. Basic model class
 */
class User {
	private $userName;
	private $pic;
	private $email;
	private $isConfirmed;
	private $role;
	private $createdAt;
	private $firstName;
	private $lastName;
	
	/*
	 * Constructor - if there is no picture associated to the user, please send null to the constructor
	 */
	function __construct($userName, $pic, $email, $isConfirmed, $role, $createdAt, $firstName, $lastName) {
		$this->userName = $userName;
		//TODO check if file exists
		$this->pic = $pic;	
		if($this->validateMail($email)) {
			$this->email = $email;
			$this->isConfirmed = $isConfirmed;
		}
		else {
			throw new Exception('mailNotValid');
			$this->email = 'INVALID';
			$this->isConfirmed = false;
		}
		$this->role = $role;
		$this->createdAt = $createdAt;
		$this->firstName = $firstName;
		$this->lastName = $lastName;
	}
	
	
	/*
	 * getName() - returns the userName
	 */
	function getUserName() {
		return $this->userName;
	}
	
	/*
	 * setUserName() - sets the userName.
	 */
	function setUserName($userName) {
		//TODO - call userDAO and change it in there as well
		$this->userName = $userName;
	}
	
	/*
	 * getPic() Returns the filename of the picture, if not set it returns null
	 */
	function getPic() {
		return $this->pic;
	}
	
	/*
	 * setPic() - remeber, send it the filename as a string
	 */
	function setPic($picFileName) {
		$this->pic = $picFileName;
	}
	
	/*
	 * getEmail - returns the mail address
	 */
	function getEmail() {
		return $this->email;
	}
	
	function setEmail($mailAddress) {
		if($this->validateMail($mailAddress)) {
			//TODO - call userDAO and change it in there as well
			$this->email = $mailAddress;
		}
		
		else throw new Exception('mailNotValid');
	}
	
	function isConfirmed() {
		if($this->isConfirmed) {
			return true;
		}
		else return false;
	}
	
	function setConfirmation($bool) {
		$this->isConfirmed = $bool;
	}
	
	function getRole() {
		return $this->role;
	}
	
	function setRole($role) {
		$this->role = $role;
	}
	
	function getCreatedAt() {
		return $this->createdAt;
	}
	
	function getNumberOfPosts() {
		//TODO Get number of posts from DB.
	}
	
	function getNumberOfComments() {
		//TODO Get number of comments from DB
	}
	
	function getFirstName() {
		return $this->firstName;
	}
	
	function setFirstName($firstName) {
		$this->firstName =$firstName;
	}
	
	function getLastName() {
		return $this->lastName;
	}
	
	function setLastName($lastName) {
		$this->lastName = $lastName;
	}
	
	
	/*
	 * validateMail - a function for validating a mail address using regular expressions
	 * found on http://www.totallyphp.co.uk/code/validate_an_email_address_using_regular_expressions.htm
	 */
	function validateMail($mail) {
		
		if(eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$", $mail)) {
			return true;
		}
		else return false;
	}
	

}
?>