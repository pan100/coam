<?php
/*
 * RSS Feed generator for COAM-CMS.
 */

$rssDOM = new DOMDocument();

$rssDOM->encoding = 'UTF-8';

$rssTag = $rssDOM->createElement("rss");
	$versionAttr = $rssDOM->createAttribute("version");
	$versionAttr->value = "2.0";
$rssTag->appendChild($versionAttr);
$rssDOM->appendChild($rssTag);

$channelElement = $rssDOM->createElement("channel");
$rssTag->appendChild($channelElement);

require_once 'controller/ConfigController.php';

	$titleElement = $rssDOM->createElement("title");
	$titleElement->nodeValue = ConfigController::getInstance()->getSitetitle() . " RSS FEED";
$channelElement->appendChild($titleElement);

	$linkElement = $rssDOM->createElement("link");
	$linkElement->nodeValue = ConfigController::getInstance()->getBaseURL();
$channelElement->appendChild($linkElement);

	$descriptionElement = $rssDOM->createElement("description");
	$descriptionElement->nodeValue = ConfigController::getInstance()->getSitedescription();
$channelElement->appendChild($descriptionElement);

//add the 10 newest posts as item elements

require_once 'controller/PostController.php';
require_once 'model/Post.php';

$tenNewest = PostController::getInstance()->getTenNewestPosts();

foreach($tenNewest as $post) {
	$itemElement = $rssDOM->createElement("item");
		$itemTitleElement = $rssDOM->createElement("title");
			$itemTitleElement->nodeValue = $post->getTitle();
	$itemElement->appendChild($itemTitleElement);
		$itemLinkElement = $rssDOM->createElement("link");
			$itemLinkElement->nodeValue = ConfigController::getInstance()->getBaseURL() . "index.php?module=ReadPost&amp;id=" . $post->getId();
	$itemElement->appendChild($itemLinkElement);
		$itemDescriptionElement = $rssDOM->createElement("description");
				$summary = $post->getContent();	
			//if the post is longer than the length of a summary set in the config, make a substring
			//from the start until position x where x is the length the summary should be, and add "..."
			if(strlen($summary) > ConfigController::getInstance()->getSummaryLength()) {
				$summary=substr($summary, 0, ConfigController::getInstance()->getSummaryLength()) . '...';
						}
			$itemDescriptionElement->nodeValue = $summary;
	$itemElement->appendChild($itemDescriptionElement);
		$itemCategoryElement = $rssDOM->createElement("category");
			$itemCategoryElement->nodeValue = $post->getCategory();
	$itemElement->appendChild($itemCategoryElement);
		$itemPubDateElement = $rssDOM->createElement("pubDate");
			$itemPubDateElement->nodeValue = $post->getCreatedAt();
	$itemElement->appendChild($itemPubDateElement);
$channelElement->appendChild($itemElement);
			
}

echo $rssDOM->saveXML();
?>