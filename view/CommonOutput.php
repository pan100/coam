<?php
require_once('view/OutputterInterface.php');
require_once('controller/ConfigController.php');
require_once('controller/CategoryController.php');


class CommonOutput implements OutputterInterface {
	private $domdocument;
	
	public function __construct($domdocument) {
		$this->domdocument = $domdocument;
		//first create the root element
		$root = $this->domdocument->createElement("coam");
		$this->domdocument->appendChild($root);
		
		//Add the title
		$title = $this->domdocument->createElement("title");
		$title->appendChild($this->domdocument->createTextNode(ConfigController::getInstance()->getSitetitle()));
		$root->appendChild($title);
		
		//Add the subtitle
		$subtitle = $this->domdocument->createElement("subtitle");
		$subtitle->appendChild($this->domdocument->createTextNode(ConfigController::getInstance()->getSitesub()));
		$root->appendChild($subtitle);
		
		//Add the description
		$description = $this->domdocument->createElement("description");
		$description->appendChild($this->domdocument->createTextNode(ConfigController::getInstance()->getSitedescription()));
		$root->appendChild($description);
		
		//Add the contact information
		$contact = $this->domdocument->createElement("contact");
		$contact->appendChild($this->domdocument->createTextNode(ConfigController::getInstance()->getSitecontact()));
		$root->appendChild($contact);
		
		//if a user is logged on, add the user as userLoggedOn
		if (!empty($_SESSION['user'])) {
 			$user = $this->domdocument->createElement("userLoggedOn");
 				$userRoleAttr = $this->domdocument->createAttribute("role");
 				$userRoleAttr->value = $_SESSION['userRole'];
 			$user->appendChild($userRoleAttr);
 				$userNameAttr = $this->domdocument->createAttribute("userName");
 				$userNameAttr->value = $_SESSION['user'];
 			$user->appendChild($userNameAttr);
 		$root->appendChild($user);
		}
		
		//then we must add the categories for the menu
		$content = $this->domdocument->createElement("content");
		$root->appendChild($content);
			$menu = $this->domdocument->createElement("menu");
			$content->appendChild($menu);
				//for each category, add a <menuCategory> with attributes numberOfPosts and name
				//NOTE I have not implemented subcategories in this version
				$categoryController = CategoryController::getInstance();
				$categories = $categoryController->getCategories();
				foreach($categories as $category) {
					$categoryTag = $this->domdocument->createElement("menuCategory");
						$categoryNameAttr = $this->domdocument->createAttribute("name");
						$categoryNameAttr->value = $category->getName();
					$categoryTag->appendChild($categoryNameAttr);
						$numPostsAttr = $this->domdocument->createAttribute("numberOfPosts");
						//Ask the controller for number of posts
						$numOfPosts = $categoryController->numberOfPosts($category);
						$numPostsAttr->value = $numOfPosts;
					$categoryTag->appendChild($numPostsAttr);
						$categoryDescriptionAttr = $this->domdocument->createAttribute("description");
						$categoryDescriptionAttr->value = $category->getDescription();
					$categoryTag->appendChild($categoryDescriptionAttr);
					$menu->appendChild($categoryTag);
				}
		$pageSpecific = $this->domdocument->createElement("pageSpecific");
		$content->appendChild($pageSpecific);
	}
	
	public function getUpdatedDOM() {
		return $this->domdocument;
	}
	
	
}
?>