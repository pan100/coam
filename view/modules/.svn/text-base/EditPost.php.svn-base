<?php
require_once 'view/OutputterInterface.php';
require_once 'controller/PostController.php';
require_once 'model/Post.php';

/*
 * get id=[the id of the post to edit] set delete to something to delete the post
 * post - title, content, category, picture, status.
 * 
 * errorMessage idNotSpecified 
 * 
 * infoMessage postDeleted
 * infoMessage postUpdated
 */

class EditPost implements OutputterInterface {
	private $domdocument;
	
	public function __construct($domdocument) {
		$this->domdocument = $domdocument;
		
			//first, get the pageSpecific tag
		$pageSpecificAsList = $domdocument->getElementsByTagName('pageSpecific');
		$pageSpecific = $pageSpecificAsList->item(0);
		//create the module tag
		$module = $this->domdocument->createElement("module");
		$pageSpecific->appendChild($module);
		
		//this is the EditPost module, so lets go right ahead and create that tag
		$editPostTag = $this->domdocument->createElement("EditPost");
		$module->appendChild($editPostTag);

		if(!isset($_GET['id'])) {
			//no post id, nothing to edit. errorMessage idNotSpecified
			$errorMessageTag = $this->domdocument->createElement("errorMessage");
				$errorMessageTypeAttr = $this->domdocument->createAttribute("type");
				$errorMessageTypeAttr->value = "idNotSpecified";
				$errorMessageTag->appendChild($errorMessageTypeAttr);
			$module->appendChild($errorMessageTag);
			
			$showSchema = $this->domdocument->createAttribute("showSchema");
			$showSchema->value = "false";
			$editPostTag->appendChild($showSchema);
		}
		
		else {
			//id is set
			//if the user is an admin or has written the post he can edit
			if($post = PostController::getInstance()->getPostById($_GET['id'])) {
											//add the correct post
				$postTag = $this->domdocument->createElement("post");
					$postIdAttr = $this->domdocument->createAttribute("id");
					$postIdAttr->value = $post->getId();
				$postTag->appendChild($postIdAttr);
					$postCategoryAttr = $this->domdocument->createAttribute("category");
					$postCategoryAttr->value = $post->getCategory();
				$postTag->appendChild($postCategoryAttr);
					$postTitleAttr = $this->domdocument->createAttribute("title");
					$postTitleAttr->value = $post->getTitle();
				$postTag->appendChild($postTitleAttr);
					$postAuthorAttr = $this->domdocument->createAttribute("author");
					$postAuthorAttr->value = $post->getUser();
				$postTag->appendChild($postAuthorAttr);
					$postStatusAttr = $this->domdocument->createAttribute("status");
					$postStatusAttr->value = $post->getStatus();
				$postTag->appendChild($postStatusAttr);
					if($post->getPicture()) {
						$postPictureAttr = $this->domdocument->createAttribute("picture");
						$postPictureAttr->value = $post->getPicture();
					$postTag->appendChild($postPictureAttr);
					}
					$postCreatedAtAttr = $this->domdocument->createAttribute("createdAt");
					$postCreatedAtAttr->value = $post->getCreatedAt();
				$postTag->appendChild($postCreatedAtAttr);
					if($post->getCreatedAt() != $post->getEditedAt()) {
						$postEditedAtAttr = $this->domdocument->createAttribute("editedAt");
						$postEditedAtAttr->value = $post->getEditedAt();
					$postTag->appendChild($postEditedAtAttr);
						$postEditedByAttr = $this->domdocument->createAttribute("editedBy");
						$postEditedByAttr->value = $post->getEditedBy();
					$postTag->appendChild($postEditedByAttr);
					}
				$postTag->appendChild($this->domdocument->createTextNode($post->getContent()));
				$editPostTag->appendChild($postTag);
			if($_SESSION['userRole'] == 'ADMIN' || $_SESSION['user'] == $post->getUser()) {
				//user is authorized
				if(isset($_POST['title'])) {
					//data was provided
					if(!empty($_POST['title']) && !empty($_POST['content']) && !empty($_POST['category']) && !empty($_POST['status'])) {
						$post->setTitle($_POST['title']);
						$post->setContent($_POST['content']);
						$post->setCategory($_POST['category']);
						$post->setStatus($_POST['status']);
						if(!empty($_POST['picture'])) {
							$post->setPicture($_POST['picture']);	
						}
						else {
							$post->setPicture(null);
						}
						PostController::getInstance()->updatePost($post);
							//infoMessage postUpdated
			$infoMessageTag = $this->domdocument->createElement("infoMessage");
				$infoMessageTypeAttr = $this->domdocument->createAttribute("type");
				$infoMessageTypeAttr->value = "postUpdated";
				$infoMessageTag->appendChild($infoMessageTypeAttr);
			$module->appendChild($infoMessageTag);
			
			$showSchema = $this->domdocument->createAttribute("showSchema");
			$showSchema->value = "false";
			$editPostTag->appendChild($showSchema);							
					}
					else {
						if(isset($_GET['delete'])) {
							PostController::getInstance()->deletePost($post);
							//infoMessage postDeleted
			$infoMessageTag = $this->domdocument->createElement("infoMessage");
				$infoMessageTypeAttr = $this->domdocument->createAttribute("type");
				$infoMessageTypeAttr->value = "postDeleted";
				$infoMessageTag->appendChild($infoMessageTypeAttr);
			$module->appendChild($infoMessageTag);
			
			$showSchema = $this->domdocument->createAttribute("showSchema");
			$showSchema->value = "false";
			$editPostTag->appendChild($showSchema);	
						}
						else {
						//errorMessage requiredFieldsMissing
			$errorMessageTag = $this->domdocument->createElement("errorMessage");
				$errorMessageTypeAttr = $this->domdocument->createAttribute("type");
				$errorMessageTypeAttr->value = "requiredFieldsMissing";
				$errorMessageTag->appendChild($errorMessageTypeAttr);
			$module->appendChild($errorMessageTag);
			
			$showSchema = $this->domdocument->createAttribute("showSchema");
			$showSchema->value = "true";
			$editPostTag->appendChild($showSchema);


					}
					}
				}
				else {
					//data was not provided. showSchema true
			$showSchema = $this->domdocument->createAttribute("showSchema");
			$showSchema->value = "true";
			$editPostTag->appendChild($showSchema);					
				}
			}
			else {
			//user not authorized. errorMessage notAuthorized
			$errorMessageTag = $this->domdocument->createElement("errorMessage");
				$errorMessageTypeAttr = $this->domdocument->createAttribute("type");
				$errorMessageTypeAttr->value = "notAuthorized";
				$errorMessageTag->appendChild($errorMessageTypeAttr);
			$module->appendChild($errorMessageTag);
			
			$showSchema = $this->domdocument->createAttribute("showSchema");
			$showSchema->value = "true";
			$editPostTag->appendChild($showSchema);				
			}
		}
		else {
			//noSuchPost
			$errorMessageTag = $this->domdocument->createElement("errorMessage");
				$errorMessageTypeAttr = $this->domdocument->createAttribute("type");
				$errorMessageTypeAttr->value = "noSuchPost";
				$errorMessageTag->appendChild($errorMessageTypeAttr);
			$module->appendChild($errorMessageTag);
			
			$showSchema = $this->domdocument->createAttribute("showSchema");
			$showSchema->value = "true";
			$editPostTag->appendChild($showSchema);	
			
		}
		}
	}
	
	public function getUpdatedDOM() {
		return $this->domdocument;
	}
}
?>