<?php
require_once 'view/OutputterInterface.php';
require_once 'controller/UserController.php';

class AdministrateUsers implements OutputterInterface {
		private $domdocument;
		
			public function __construct($domdocument) {
		$this->domdocument = $domdocument;
		//first, get the pageSpecific tag
		$pageSpecificAsList = $domdocument->getElementsByTagName('pageSpecific');
		$pageSpecific = $pageSpecificAsList->item(0);
		//create the module tag
		$module = $this->domdocument->createElement("module");
		$pageSpecific->appendChild($module);
		
		//this is the AdministrateUsers module, so lets go right ahead and create that tag
		$administrateUsersTag = $this->domdocument->createElement("AdministrateUsers");
		$module->appendChild($administrateUsersTag);
		
		if(isset($_SESSION['user']) && $_SESSION['userRole'] == 'ADMIN') {
			$users = UserController::getInstance()->getAllUsers();
			
			foreach($users as $user) {
								$userTag = $this->domdocument->createElement("user");
					$userUserNameAttr = $this->domdocument->createAttribute("userName");
					$userUserNameAttr->value = $user->getUserName();
				$userTag->appendChild($userUserNameAttr);
					$userRoleAttr = $this->domdocument->createAttribute("role");
					$userRoleAttr->value = $user->getRole();
				$userTag->appendChild($userRoleAttr);
					$userEMailAttr = $this->domdocument->createAttribute("eMail");
					$userEMailAttr->value = $user->getEmail();
				$userTag->appendChild($userEMailAttr);
					$userFirstNameAttr = $this->domdocument->createAttribute("firstName");
					$userFirstNameAttr->value = $user->getFirstName();
				$userTag->appendChild($userFirstNameAttr);
					$userLastNameAttr = $this->domdocument->createAttribute("lastName");
					$userLastNameAttr->value = $user->getLastName();
				$userTag->appendChild($userLastNameAttr);
					$userIsConfirmedAttr = $this->domdocument->createAttribute("isConfirmed");
					if($user->isConfirmed()) {
						$userIsConfirmedAttr->value = "true";
					}
					else {
						$userIsConfirmedAttr->value = "false";
					}
				$userTag->appendChild($userIsConfirmedAttr);
					if($user->getPic()) {
						$userPictureFileAttr = $this->domdocument->createAttribute("pictureFile");
						$userPictureFileAttr->value = $user->getPic();
					$userTag->appendChild($userPictureFileAttr);
					}
					$userCreatedAtAttr = $this->domdocument->createAttribute("createdAt");
					$userCreatedAtAttr->value = $user->getCreatedAt();
				$userTag->appendChild($userCreatedAtAttr);
				
				$administrateUsersTag->appendChild($userTag);
			}
		}
		else {
			//errorMessage notAuthorized
			$errorMessageTag = $this->domdocument->createElement("errorMessage");
				$errorMessageTypeAttr = $this->domdocument->createAttribute("type");
				$errorMessageTypeAttr->value = "notAuthorized";
				$errorMessageTag->appendChild($errorMessageTypeAttr);
			$module->appendChild($errorMessageTag);
		}
			}
	
	public function getUpdatedDOM() {
		return $this->domdocument;
	}
}
?>