<?php
require_once 'view/OutputterInterface.php';

class AdministratorMenu implements OutputterInterface {
		private $domdocument;

		function __construct($domdocument) {
	
		$this->domdocument = $domdocument;
		
			//first, get the pageSpecific tag
		$pageSpecificAsList = $domdocument->getElementsByTagName('pageSpecific');
		$pageSpecific = $pageSpecificAsList->item(0);
		//create the module tag
		$module = $this->domdocument->createElement("module");
		$pageSpecific->appendChild($module);
		
		//this is the AdministratorMenu module, so lets go right ahead and create that tag
		$administratorMenuTag = $this->domdocument->createElement("AdministratorMenu");
		$module->appendChild($administratorMenuTag);
		
		}
		
		public function getUpdatedDOM() {
			return $this->domdocument;
		}
}
?>