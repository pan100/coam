<?php
require_once 'controller/ConfigController.php';
require_once 'view/OutputterInterface.php';

/*
 * POST title, sub, description, contact and summary
 * 
 * introduces infoMessage configChanged
 */

class ConfigureCOAM implements OutputterInterface {
	private $domdocument;
	
	public function __construct($domdocument) {
		$this->domdocument = $domdocument;
		
			//first, get the pageSpecific tag
		$pageSpecificAsList = $domdocument->getElementsByTagName('pageSpecific');
		$pageSpecific = $pageSpecificAsList->item(0);
		//create the module tag
		$module = $this->domdocument->createElement("module");
		$pageSpecific->appendChild($module);
		
		//this is the ConfigureCOAM module, so lets go right ahead and create that tag
		$configureCOAMTag = $this->domdocument->createElement("ConfigureCOAM");
		$module->appendChild($configureCOAMTag);
		
		if(isset($_SESSION['user']) && $_SESSION['userRole'] == 'ADMIN') {
			//user is authorized
			$summaryLengthAttr = $this->domdocument->createAttribute("summaryLength");
			$summaryLengthAttr->value = ConfigController::getInstance()->getSummaryLength();
		$configureCOAMTag->appendChild($summaryLengthAttr);
			if(isset($_POST['title'])) {
				//data was submitted. Check for changes, apply them and save the config.
				$changedConfig = false;
				
				if(!empty($_POST['title'])) {
					if(ConfigController::getInstance()->getSitetitle() != $_POST['title']) {
						ConfigController::getInstance()->setSitetitle($_POST['title']);
						$changedConfig = true;
					}
				}
				if(!empty($_POST['sub'])) {
					if(ConfigController::getInstance()->getSitesub() != $_POST['sub']) {
						ConfigController::getInstance()->setSitesub($_POST['sub']);
						$changedConfig = true;
					}
				}
				if(!empty($_POST['description'])) {
					if(ConfigController::getInstance()->getSitedescription() != $_POST['description']) {
						ConfigController::getInstance()->setSitedescription($_POST['description']);
						$changedConfig = true;
					}
				}
				if(!empty($_POST['contact'])) {
					if(ConfigController::getInstance()->getSitecontact() != $_POST['contact']) {
						ConfigController::getInstance()->setSitecontact($_POST['contact']);
						$changedConfig = true;
					}
				}
				if(!empty($_POST['summary'])) {
					if(ConfigController::getInstance()->getSummaryLength() != $_POST['summary']) {
						ConfigController::getInstance()->setSummaryLength($_POST['summary']);
						$changedConfig = true;
					}
				}
				
				if($changedConfig) {
					ConfigController::getInstance()->saveChanges();
					//infoMessage configChanged
				$infoMessageTag = $this->domdocument->createElement("infoMessage");
				$infoMessageTypeAttr = $this->domdocument->createAttribute("type");
				$infoMessageTypeAttr->value = "configChanged";
				$infoMessageTag->appendChild($infoMessageTypeAttr);
			$pageSpecific->appendChild($infoMessageTag);
				}
				else {
				//no changes were made. errorMessage noChanges
			$errorMessageTag = $this->domdocument->createElement("errorMessage");
				$errorMessageTypeAttr = $this->domdocument->createAttribute("type");
				$errorMessageTypeAttr->value = "noChanges";
				$errorMessageTag->appendChild($errorMessageTypeAttr);
			$pageSpecific->appendChild($errorMessageTag);						
				}
			}
		}
		else {
			//errorMessage notAuthorized
			$errorMessageTag = $this->domdocument->createElement("errorMessage");
				$errorMessageTypeAttr = $this->domdocument->createAttribute("type");
				$errorMessageTypeAttr->value = "notAuthorized";
				$errorMessageTag->appendChild($errorMessageTypeAttr);
			$module->appendChild($errorMessageTag);
		}
		
		
	}
	
	public function getUpdatedDOM() {
		return $this->domdocument;
	}
}
?>