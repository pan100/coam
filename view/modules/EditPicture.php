<?php
require_once 'view/OutputterInterface.php';

//NOT IMPLEMENTED DUE TO LACK OF TIME

class EditPicture implements OutputterInterface {
		private $domdocument;

		function __construct($domdocument) {
	
		$this->domdocument = $domdocument;
		
			//first, get the pageSpecific tag
		$pageSpecificAsList = $domdocument->getElementsByTagName('pageSpecific');
		$pageSpecific = $pageSpecificAsList->item(0);
		//create the module tag
		$module = $this->domdocument->createElement("module");
		$pageSpecific->appendChild($module);
		
		//this is the EditPicture module, so lets go right ahead and create that tag
		$editPictureTag = $this->domdocument->createElement("EditPicture");
		$module->appendChild($editPictureTag);
		
		}
		
		public function getUpdatedDOM() {
			return $this->domdocument;
		}
}
?>