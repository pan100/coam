<?php
//TODO complete rewrite of this class
require_once 'view/OutputterInterface.php';
require_once 'model/User.php';
require_once 'controller/UserController.php';

/*
 * provide username through a get. You can set the first name through post setFirstName, last name through post setLastName,
 * picture through post setPictureFile (must be uploaded by the user who are being edited), e-mail through post setEmail,
 * password through post setPassword (old password must be provided in post oldPassword and the new password must also be sent
 * in setPasswordRepeat, to make sure the user does not do a typo in the new password.
 * ), and confirmation status through
 * setConfirmed (true or false)
 * 
 * you can also use get action=confirm on a user that is not confirmed, if you are an admin
 * 
 * A user cannot be deleted in this early version, setConfirmed to false to make them unable to log in
 * 
 * 
 * errorMessages:
 * type=userNotSpecified
 * type=noSuchUser
 * type=notLoggedIn
 * type=notAuthorized
 * type=noPasswordProvided
 * type=wrongPassword
 * type=passwordNotRepeated
 * type=repeatPassTypo
 * type=invalidInput
 * type=noChanges
 * 
 * infoMessages:
 * 
 * passwordChanged
 * userEdited
 * 
 */
class EditUser implements OutputterInterface {
	
	private $domdocument;
	
	function __construct($domdocument) {
		$this->domdocument = $domdocument;
		
			//first, get the pageSpecific tag
		$pageSpecificAsList = $domdocument->getElementsByTagName('pageSpecific');
		$pageSpecific = $pageSpecificAsList->item(0);
		//create the module tag
		$module = $this->domdocument->createElement("module");
		$pageSpecific->appendChild($module);
		
		//this is the EditUser module, so lets go right ahead and create that tag
		$editUserTag = $this->domdocument->createElement("EditUser");
		$module->appendChild($editUserTag);
		
				//if a username was not provided, make an errorMessage
		
		if(!isset($_GET['username'])) {
			$errorMessageTag = $this->domdocument->createElement("errorMessage");
				$errorMessageTypeAttr = $this->domdocument->createAttribute("type");
				$errorMessageTypeAttr->value = "userNotSpecified";
				$errorMessageTag->appendChild($errorMessageTypeAttr);
			$module->appendChild($errorMessageTag);
		}
		
		else {
			//username was provided
			if(isset($_SESSION['user'])) {
				//the user who requests to edit is logged on
				if($user = UserController::getInstance()->getUserByUserName($_GET['username'])) {
					//the user exists
				if($_SESSION['userRole'] == 'ADMIN' || $_SESSION['user'] == $_GET['username']) {
					//the user is either an admin or he wants to edit his own user profile. This means he is authorized
					if(!empty($_POST['setPassword'])) {
						//the password field is set
						if(!(!empty($_POST['oldPassword']))) {
							//the old password was not provided. errorMessage noPasswordProvided
										$errorMessageTag = $this->domdocument->createElement("errorMessage");
				$errorMessageTypeAttr = $this->domdocument->createAttribute("type");
				$errorMessageTypeAttr->value = "noPasswordProvided";
				$errorMessageTag->appendChild($errorMessageTypeAttr);
			$pageSpecific->appendChild($errorMessageTag);
							
						}
						else if(!(!empty($_POST['setPasswordRepeat']))) {
							//the password was not repeated. errorMessage passwordNotRepeated
										$errorMessageTag = $this->domdocument->createElement("errorMessage");
				$errorMessageTypeAttr = $this->domdocument->createAttribute("type");
				$errorMessageTypeAttr->value = "passwordNotRepeated";
				$errorMessageTag->appendChild($errorMessageTypeAttr);
			$pageSpecific->appendChild($errorMessageTag);
						}
						else {

							//all three required password fields are set
							if($_POST['setPassword'] != $_POST['setPasswordRepeat']) {
								//the new password was not repeated correctly. errorMessage repeatPassTypo
											$errorMessageTag = $this->domdocument->createElement("errorMessage");
				$errorMessageTypeAttr = $this->domdocument->createAttribute("type");
				$errorMessageTypeAttr->value = "repeatePassTypo";
				$errorMessageTag->appendChild($errorMessageTypeAttr);
			$pageSpecific->appendChild($errorMessageTag);
							}
							else if(!UserController::getInstance()->validateUser($_GET['username'], $_POST['oldPassword'])) {
								//the old password was not correct. errorMessage wrongPassword
											$errorMessageTag = $this->domdocument->createElement("errorMessage");
				$errorMessageTypeAttr = $this->domdocument->createAttribute("type");
				$errorMessageTypeAttr->value = "wrongPassword";
				$errorMessageTag->appendChild($errorMessageTypeAttr);
			$pageSpecific->appendChild($errorMessageTag);
							}
							else {
								//go ahead and change the password, everything is ok. infoMessage passwordChanged
								UserController::getInstance()->setPassword($_GET['username'], $_POST['setPassword']);
							$infoMessageTag = $this->domdocument->createElement("infoMessage");
				$infoMessageTypeAttr = $this->domdocument->createAttribute("type");
				$infoMessageTypeAttr->value = "passwordChanged";
				$infoMessageTag->appendChild($infoMessageTypeAttr);
			$pageSpecific->appendChild($infoMessageTag);	
			$passwordSet = true;
							}
						}
						if(!empty($_POST['setFirstName']) || !empty($_POST['setLastName']) || !empty($_POST['setPictureFile'])
						 || !empty($_POST['setEmail']) || !empty($_POST['setConfirmed'])) {
					 	//at least one of the setters were provided
					 	//if some of the input is invalid, errorMessage invalidInput
					 	if(!$this->settersAreValid()) {
											$errorMessageTag = $this->domdocument->createElement("errorMessage");
				$errorMessageTypeAttr = $this->domdocument->createAttribute("type");
				$errorMessageTypeAttr->value = "invalidInput";
				$errorMessageTag->appendChild($errorMessageTypeAttr);
			$pageSpecific->appendChild($errorMessageTag);					 		
					 	}
					 	else {
					 		
					 		//make the changes and send them to the controller
					 		$userToModify = UserController::getInstance()->getUserByUserName($_GET['username']);
					 		
					if(!empty($_POST['setFirstName'])) {
						$userToModify->setFirstName($_POST['setFirstName']);
					}
					if(!empty($_POST['setLastName'])) {
						$userToModify->setLastName($_POST['setLastName']);
					}
					if(!empty($_POST['setPictureFile'])) {
						$userToModify->setPic($_POST['setPictureFile']);
					}
					if(!empty($_POST['setEmail'])) {
						$userToModify->setEmail($_POST['setEmail']);
					}
					if(!empty($_POST['setConfirmed'])) {
						if($_POST['setConfirmed'] == 'true') {
							$userToModify->setConfirmation(true);
						}
						else {
							$userToModify->setConfirmation(false);
						}
					}		

					if(UserController::getInstance()->updateUser($userToModify))    {
							//infoMessage userEdited
				$infoMessageTag = $this->domdocument->createElement("infoMessage");
				$infoMessageTypeAttr = $this->domdocument->createAttribute("type");
				$infoMessageTypeAttr->value = "userEdited";
				$infoMessageTag->appendChild($infoMessageTypeAttr);
			$pageSpecific->appendChild($infoMessageTag);
							}
							else {
								if(!($passwordSet)) {
								//no changes were made. errorMessage noChanges
											$errorMessageTag = $this->domdocument->createElement("errorMessage");
				$errorMessageTypeAttr = $this->domdocument->createAttribute("type");
				$errorMessageTypeAttr->value = "noChanges";
				$errorMessageTag->appendChild($errorMessageTypeAttr);
			$pageSpecific->appendChild($errorMessageTag);								
								//add the user tag as a child of the EditUser tag
				$userTag = $this->domdocument->createElement("user");
					$userUserNameAttr = $this->domdocument->createAttribute("userName");
					$userUserNameAttr->value = $user->getUserName();
				$userTag->appendChild($userUserNameAttr);
					$userRoleAttr = $this->domdocument->createAttribute("role");
					$userRoleAttr->value = $user->getRole();
				$userTag->appendChild($userRoleAttr);
					$userEMailAttr = $this->domdocument->createAttribute("eMail");
					$userEMailAttr->value = $user->getEmail();
				$userTag->appendChild($userEMailAttr);
					$userFirstNameAttr = $this->domdocument->createAttribute("firstName");
					$userFirstNameAttr->value = $user->getFirstName();
				$userTag->appendChild($userFirstNameAttr);
					$userLastNameAttr = $this->domdocument->createAttribute("lastName");
					$userLastNameAttr->value = $user->getLastName();
				$userTag->appendChild($userLastNameAttr);
					$userIsConfirmedAttr = $this->domdocument->createAttribute("isConfirmed");
					if($user->isConfirmed()) {
						$userIsConfirmedAttr->value = "true";
					}
					else {
						$userIsConfirmedAttr->value = "false";
					}
				$userTag->appendChild($userIsConfirmedAttr);
					if($user->getPic()) {
						$userPictureFileAttr = $this->domdocument->createAttribute("pictureFile");
						$userPictureFileAttr->value = $user->getPic();
					$userTag->appendChild($userPictureFileAttr);
					}
					$userCreatedAtAttr = $this->domdocument->createAttribute("createdAt");
					$userCreatedAtAttr->value = $user->getCreatedAt();
				$userTag->appendChild($userCreatedAtAttr);
				
				$editUserTag->appendChild($userTag);
							}
							}
					 	}
						
						}
					}
					else {
						//the password field was not set

						if(!empty($_POST['setFirstName']) || !empty($_POST['setLastName']) || !empty($_POST['setPictureFile'])
						 || !empty($_POST['setEmail']) || !empty($_POST['setConfirmed'])) {
					 	//at least one of the setters were provided
					 	//if some of the input is invalid, errorMessage invalidInput
					 	if(!$this->settersAreValid()) {
											$errorMessageTag = $this->domdocument->createElement("errorMessage");
				$errorMessageTypeAttr = $this->domdocument->createAttribute("type");
				$errorMessageTypeAttr->value = "invalidInput";
				$errorMessageTag->appendChild($errorMessageTypeAttr);
			$pageSpecific->appendChild($errorMessageTag);					 		
					 	}
					 	else {
					 		
					 		//make the changes and send them to the controller
					 		$userToModify = UserController::getInstance()->getUserByUserName($_GET['username']);
					 		
					if(!empty($_POST['setFirstName'])) {
						$userToModify->setFirstName($_POST['setFirstName']);
					}
					if(!empty($_POST['setLastName'])) {
						$userToModify->setLastName($_POST['setLastName']);
					}
					if(!empty($_POST['setPictureFile'])) {
						$userToModify->setPic($_POST['setPictureFile']);
					}
					if(!empty($_POST['setEmail'])) {
						$userToModify->setEmail($_POST['setEmail']);
					}
					if(!empty($_POST['setConfirmed'])) {
						if($_POST['setConfirmed'] == 'true') {
							$userToModify->setConfirmation(true);
						}
						else {
							$userToModify->setConfirmation(false);
						}
					}		

					if(UserController::getInstance()->updateUser($userToModify))    {
							//infoMessage userEdited
				$infoMessageTag = $this->domdocument->createElement("infoMessage");
				$infoMessageTypeAttr = $this->domdocument->createAttribute("type");
				$infoMessageTypeAttr->value = "userEdited";
				$infoMessageTag->appendChild($infoMessageTypeAttr);
			$pageSpecific->appendChild($infoMessageTag);
							}
							else {
								//no changes were made. errorMessage noChanges
											$errorMessageTag = $this->domdocument->createElement("errorMessage");
				$errorMessageTypeAttr = $this->domdocument->createAttribute("type");
				$errorMessageTypeAttr->value = "noChanges";
				$errorMessageTag->appendChild($errorMessageTypeAttr);
			$pageSpecific->appendChild($errorMessageTag);								
								//add the user tag as a child of the EditUser tag
				$userTag = $this->domdocument->createElement("user");
					$userUserNameAttr = $this->domdocument->createAttribute("userName");
					$userUserNameAttr->value = $user->getUserName();
				$userTag->appendChild($userUserNameAttr);
					$userRoleAttr = $this->domdocument->createAttribute("role");
					$userRoleAttr->value = $user->getRole();
				$userTag->appendChild($userRoleAttr);
					$userEMailAttr = $this->domdocument->createAttribute("eMail");
					$userEMailAttr->value = $user->getEmail();
				$userTag->appendChild($userEMailAttr);
					$userFirstNameAttr = $this->domdocument->createAttribute("firstName");
					$userFirstNameAttr->value = $user->getFirstName();
				$userTag->appendChild($userFirstNameAttr);
					$userLastNameAttr = $this->domdocument->createAttribute("lastName");
					$userLastNameAttr->value = $user->getLastName();
				$userTag->appendChild($userLastNameAttr);
					$userIsConfirmedAttr = $this->domdocument->createAttribute("isConfirmed");
					if($user->isConfirmed()) {
						$userIsConfirmedAttr->value = "true";
					}
					else {
						$userIsConfirmedAttr->value = "false";
					}
				$userTag->appendChild($userIsConfirmedAttr);
					if($user->getPic()) {
						$userPictureFileAttr = $this->domdocument->createAttribute("pictureFile");
						$userPictureFileAttr->value = $user->getPic();
					$userTag->appendChild($userPictureFileAttr);
					}
					$userCreatedAtAttr = $this->domdocument->createAttribute("createdAt");
					$userCreatedAtAttr->value = $user->getCreatedAt();
				$userTag->appendChild($userCreatedAtAttr);
				
				$editUserTag->appendChild($userTag);
								
							}
					 	}
												
						}
						else {
							if(isset($_GET['action']) && $_GET['action'] == 'confirm') {
								$user = UserController::getInstance()->getUserByUserName($_GET['username']);
								if(!$user->isConfirmed()) {
									$user->setConfirmation(true);
									UserController::getInstance()->updateUser($user);
									
													$infoMessageTag = $this->domdocument->createElement("infoMessage");
				$infoMessageTypeAttr = $this->domdocument->createAttribute("type");
				$infoMessageTypeAttr->value = "userEdited";
				$infoMessageTag->appendChild($infoMessageTypeAttr);
			$pageSpecific->appendChild($infoMessageTag);
									
								}
							}
							//only username was provided. add the user tag as a child of the EditUser tag
							$user = UserController::getInstance()->getUserByUserName($_GET['username']);
							
											//make the user tag and all the attributes
				$userTag = $this->domdocument->createElement("user");
					$userUserNameAttr = $this->domdocument->createAttribute("userName");
					$userUserNameAttr->value = $user->getUserName();
				$userTag->appendChild($userUserNameAttr);
					$userRoleAttr = $this->domdocument->createAttribute("role");
					$userRoleAttr->value = $user->getRole();
				$userTag->appendChild($userRoleAttr);
					$userEMailAttr = $this->domdocument->createAttribute("eMail");
					$userEMailAttr->value = $user->getEmail();
				$userTag->appendChild($userEMailAttr);
					$userFirstNameAttr = $this->domdocument->createAttribute("firstName");
					$userFirstNameAttr->value = $user->getFirstName();
				$userTag->appendChild($userFirstNameAttr);
					$userLastNameAttr = $this->domdocument->createAttribute("lastName");
					$userLastNameAttr->value = $user->getLastName();
				$userTag->appendChild($userLastNameAttr);
					$userIsConfirmedAttr = $this->domdocument->createAttribute("isConfirmed");
					if($user->isConfirmed()) {
						$userIsConfirmedAttr->value = "true";
					}
					else {
						$userIsConfirmedAttr->value = "false";
					}
				$userTag->appendChild($userIsConfirmedAttr);
					if($user->getPic()) {
						$userPictureFileAttr = $this->domdocument->createAttribute("pictureFile");
						$userPictureFileAttr->value = $user->getPic();
					$userTag->appendChild($userPictureFileAttr);
					}
					$userCreatedAtAttr = $this->domdocument->createAttribute("createdAt");
					$userCreatedAtAttr->value = $user->getCreatedAt();
				$userTag->appendChild($userCreatedAtAttr);
				
				$editUserTag->appendChild($userTag);
								
						}
					}
					
				}
				else {
				//the user is not an admin and tries to edit someone elses profile. errorMessage notAuthorized
							$errorMessageTag = $this->domdocument->createElement("errorMessage");
				$errorMessageTypeAttr = $this->domdocument->createAttribute("type");
				$errorMessageTypeAttr->value = "notAuthorized";
				$errorMessageTag->appendChild($errorMessageTypeAttr);
			$pageSpecific->appendChild($errorMessageTag);
				}
			}
			else {
				//the user does not exist. errorMessage noSuchUser
											$errorMessageTag = $this->domdocument->createElement("errorMessage");
				$errorMessageTypeAttr = $this->domdocument->createAttribute("type");
				$errorMessageTypeAttr->value = "noSuchUser";
				$errorMessageTag->appendChild($errorMessageTypeAttr);
			$pageSpecific->appendChild($errorMessageTag);
			}
				
			}
			else {
				//the user who wants to edit is not logged on. errorMessage notLoggedIn
			$errorMessageTag = $this->domdocument->createElement("errorMessage");
				$errorMessageTypeAttr = $this->domdocument->createAttribute("type");
				$errorMessageTypeAttr->value = "notLoggedIn";
				$errorMessageTag->appendChild($errorMessageTypeAttr);
			$pageSpecific->appendChild($errorMessageTag);
			}
		}
		
		
	}
	
	public function getUpdatedDOM() {
		return $this->domdocument;
	}
	
	/*
	 * isset($_POST['setFirstName']) || isset($_POST['setLastName']) || isset($_POST['setPictureFile'])
						 || isset($_POST['setEmail']) || isset($_POST['setConfirmed'])
	 */
	private function settersAreValid() {
		if(!empty($_POST['setFirstName'])) {
			if(strlen($_POST['setFirstName']) >= 30 ) {
				return false;
			}
		}
		if(!empty($_POST['setLastName'])) {
			if(strlen($_POST['setLastName']) >= 30 ) {
				return false;
			}
		}
		if(!empty($_POST['setPictureFile'])) {
			if(strlen($_POST['setPictureFile']) >= 30 ) {
				return false;
			}
		}
			if(!empty($_POST['setEmail'])) {
				//TODO a weakness here is that if the mail is not validated through this regex the user just gets a general error.
				if(!(eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$", $_POST['setEmail']))) {
				return false;
				}
				if(strlen($_POST['setEmail']) >= 40 ) {
				return false;
			}
		}
				if(!empty($_POST['setConfirmed'])) {
					if(!($_POST['setConfirmed'] == 'true' || $_POST['setConfirmed'] == 'false')) {
						return false;
			}
		}
		return true;
	}
	
}
?>