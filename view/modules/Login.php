<?php
/*
 * Must be given through a POST form: user, pass
 * This one can give errorMessages:
 * type=noPasswordProvided
 * type=wrongPassword
 * type=noSuchUser
 * type=notConfirmed
 * 
 * infoMessage:
 * type=userLoggedOn
 * 
 */

//TODO design - legg inn notConfirmed

require_once 'controller/UserController.php';
require_once 'model/User.php';

class Login implements OutputterInterface {
	private $domdocument;
	
	function __construct($domdocument) {
		$this->domdocument =$domdocument;
				//first, get the pageSpecific tag
		$pageSpecificAsList = $domdocument->getElementsByTagName('pageSpecific');
		$pageSpecific = $pageSpecificAsList->item(0);
		//create the module tag
		$module = $this->domdocument->createElement("module");
		$pageSpecific->appendChild($module);
		
		//this is the Login module, so lets go right ahead and create that tag
		$loginTag = $this->domdocument->createElement("Login");
		$module->appendChild($loginTag);
		
		//Login has one required attribute, and that is showSchema. It must be true or false.
		//The only time it is false is if a user was successfully logged in.
		if(isset($_POST['user'])) {
			// someone is trying to log in
			if(!(isset($_POST['pass']))) {
				//no password was provided. Give noPasswordProvided errorMessage and create showSchema. Set it to true.
				$errorMessageTag = $this->domdocument->createElement("errorMessage");
					$errorMessageTypeAttr = $this->domdocument->createAttribute("type");
					$errorMessageTypeAttr->value = "noPasswordProvided";
				$errorMessageTag->appendChild($errorMessageTypeAttr);
			$pageSpecific->appendChild($errorMessageTag);
			
			$showSchema = $this->domdocument->createAttribute("showSchema");
			$showSchema->value = "true";
			$loginTag->appendChild($showSchema);
			}
			else {
				//check if the user exists and is confirmed
				if($existingUser = UserController::getInstance()->getUserByUserName($_POST['user'])) {
					if($existingUser->isConfirmed()) {
						//the user exists and is confirmed- try to log on
					if(UserController::getInstance()->login($_POST['user'], $_POST['pass'])) {
						//success -> give a userLoggedOn infoMessage. Create showSchema-false
					$infoMessageTag = $this->domdocument->createElement("infoMessage");
						$infoMessageTypeAttr = $this->domdocument->createAttribute("type");
						$infoMessageTypeAttr->value = "userLoggedOn";
					$infoMessageTag->appendChild($infoMessageTypeAttr);
					$pageSpecific->appendChild($infoMessageTag);
			
					$showSchema = $this->domdocument->createAttribute("showSchema");
					$showSchema->value = "false";
						
						//get the rootElement from the domdocument and add the userLoggedOn element.
					$rootElement = $this->domdocument->getElementsByTagName("coam")->item(0);
						$userLoggedOn = $this->domdocument->createElement("userLoggedOn");
							$roleAttr = $this->domdocument->createAttribute("role");
							$roleAttr->value = $_SESSION['userRole'];
						$userLoggedOn->appendChild($roleAttr);
							$userNameAttr = $this->domdocument->createAttribute("userName");
							$userNameAttr->value = $_SESSION['user'];
						$userLoggedOn->appendChild($userNameAttr);
					$rootElement->appendChild($userLoggedOn);
					}
						else {
							//if it fails, errorMessage wrongPassword. ShowSchema-true
							$errorMessageTag = $this->domdocument->createElement("errorMessage");
								$errorMessageTypeAttr = $this->domdocument->createAttribute("type");
								$errorMessageTypeAttr->value = "wrongPassword";
							$errorMessageTag->appendChild($errorMessageTypeAttr);
						$pageSpecific->appendChild($errorMessageTag);
			
						$showSchema = $this->domdocument->createAttribute("showSchema");
						$showSchema->value = "true";
						$loginTag->appendChild($showSchema);	
						}			
					}
					else {
						//the user is not confirmed - errorMessage notConfirmed - ShowSchema-true
					$errorMessageTag = $this->domdocument->createElement("errorMessage");
						$errorMessageTypeAttr = $this->domdocument->createAttribute("type");
						$errorMessageTypeAttr->value = "notConfirmed";
					$errorMessageTag->appendChild($errorMessageTypeAttr);
						$errorMessageAdditionalInfoAttr = $this->domdocument->createAttribute("additionalInfo");
						$errorMessageAdditionalInfoAttr->value = $_POST['user'];
					$errorMessageTag->appendChild($errorMessageAdditionalInfoAttr);
				$pageSpecific->appendChild($errorMessageTag);
					$showSchema = $this->domdocument->createAttribute("showSchema");
					$showSchema->value = "true";
					$loginTag->appendChild($showSchema);						
					}
						
				}
				else {
					//if it doesn't exist, noSuchUser errorMessage. showSchema-true
					$errorMessageTag = $this->domdocument->createElement("errorMessage");
						$errorMessageTypeAttr = $this->domdocument->createAttribute("type");
						$errorMessageTypeAttr->value = "noSuchUser";
					$errorMessageTag->appendChild($errorMessageTypeAttr);
						$errorMessageAdditionalInfoAttr = $this->domdocument->createAttribute("additionalInfo");
						$errorMessageAdditionalInfoAttr->value = $_POST['user'];
					$errorMessageTag->appendChild($errorMessageAdditionalInfoAttr);
				$pageSpecific->appendChild($errorMessageTag);
					$showSchema = $this->domdocument->createAttribute("showSchema");
					$showSchema->value = "true";
					$loginTag->appendChild($showSchema);					
				} 
			}
		}
		else {
			//no one is trying to log in, they simply want to see the login form. ShowSchema-true
			$showSchema = $this->domdocument->createAttribute("showSchema");
			$showSchema->value = "true";
			$loginTag->appendChild($showSchema);
		}
	}
	
	public function getUpdatedDOM() {
		return $this->domdocument;
	}
}
?>