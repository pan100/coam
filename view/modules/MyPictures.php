<?php
require_once 'view/OutputterInterface.php';
require_once 'controller/PictureController.php';

class MyPictures implements OutputterInterface {
	private $domdocument;
	
	public function __construct($domdocument) {
			$this->domdocument = $domdocument;

		$pageSpecificAsList = $domdocument->getElementsByTagName('pageSpecific');
		$pageSpecific = $pageSpecificAsList->item(0);
		//create the module tag
		$module = $this->domdocument->createElement("module");
		$pageSpecific->appendChild($module);
		
		//this is the MyPictures module, so lets go right ahead and create that tag
		$myPicturesTag = $this->domdocument->createElement("MyPictures");
		$module->appendChild($myPicturesTag);
		
		if(isset($_SESSION['user'])) {
			$pictures = PictureController::getInstance()->getPicturesByUploader($_SESSION['user']);
			
			foreach($pictures as $picture) {
				$pictureTag = $this->domdocument->createElement("picture");
					/*
					 * 						<!ATTLIST picture fileName CDATA #REQUIRED>
						<!ATTLIST picture title CDATA #REQUIRED>
						<!ATTLIST picture uploadedBy CDATA #REQUIRED>
						<!ATTLIST picture lastChanged CDATA #REQUIRED>
					 */
					$pictureFileNameAttr = $this->domdocument->createAttribute("fileName");
					$pictureFileNameAttr->value = $picture->getFileName();
				$pictureTag->appendChild($pictureFileNameAttr);
					$pictureTitleAttr = $this->domdocument->createAttribute("title");
					$pictureTitleAttr->value = $picture->getTitle();
				$pictureTag->appendChild($pictureTitleAttr);
					$pictureUploadedByAttr = $this->domdocument->createAttribute("uploadedBy");
					$pictureUploadedByAttr->value = $picture->getUploadedBy();
				$pictureTag->appendChild($pictureUploadedByAttr);
					$pictureLastChangedAttr = $this->domdocument->createAttribute("lastChanged");
					$pictureLastChangedAttr->value = $picture->getLastChanged();
				$pictureTag->appendChild($pictureLastChangedAttr);
			$myPicturesTag->appendChild($pictureTag);
			}
		}
	}
	
	public function getUpdatedDOM() {
		return $this->domdocument;
	}
}
?>