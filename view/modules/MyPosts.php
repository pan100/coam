<?php
require_once 'controller/PostController.php';
require_once 'controller/UserController.php';
require_once 'view/OutputterInterface.php';
require_once 'model/Post.php';

//give this a get user=[username] and it shows the posts of another user

/*
 * 
 */

class MyPosts implements OutputterInterface {
	private $domdocument;
	
	public function __construct($domdocument) {
		$this->domdocument = $domdocument;

		$pageSpecificAsList = $domdocument->getElementsByTagName('pageSpecific');
		$pageSpecific = $pageSpecificAsList->item(0);
		//create the module tag
		$module = $this->domdocument->createElement("module");
		$pageSpecific->appendChild($module);
		
		//this is the MyPosts module, so lets go right ahead and create that tag
		$myPostsTag = $this->domdocument->createElement("MyPosts");
		$module->appendChild($myPostsTag);
		
		if(isset($_GET['username'])) {
			//a specific username was provided
			if(UserController::getInstance()->userExists($_GET['username'])) {
				//the user specified exists. List the posts
				$posts = PostController::getInstance()->getPostedPostsByUserName($_GET['username']);
				
										foreach($posts as $post) {
					$iPost = $this->domdocument->createElement("iPost");
						$iPostIdAttr = $this->domdocument->createAttribute("id");
						$iPostIdAttr->value = $post->getId();
							$iPost->appendChild($iPostIdAttr);
						$iPostTitleAttr = $this->domdocument->createAttribute("title");
						$iPostTitleAttr->value = $post->getTitle();
							$iPost->appendChild($iPostTitleAttr);
						$iPostAuthorAttr = $this->domdocument->createAttribute("author");
						$iPostAuthorAttr->value = $post->getUser();
							$iPost->appendChild($iPostAuthorAttr);
							
							if($post->getPicture()) {
						$iPostPictureAttr = $this->domdocument->createAttribute("picture");
						$iPostPictureAttr->value = $post->getPicture();
							$iPost->appendChild($iPostPictureAttr);
							}
							
						$iPostCreatedAtAttr = $this->domdocument->createAttribute("createdAt");
						$iPostCreatedAtAttr->value = $post->getCreatedAt();
							$iPost->appendChild($iPostCreatedAtAttr);
							
							if(!($post->getCreatedAt() == $post->getEditedAt())) {
						$iPostEditedAtAttr = $this->domdocument->createAttribute("editedAt");
						$iPostEditedAtAttr->value = $post->getEditedAt();
							$iPost->appendChild($iPostEditedAtAttr);
						$iPostEditedByAttr = $this->domdocument->createAttribute("editedBy");
						$iPostEditedByAttr->value = $post->getEditedBy();
							$iPost->appendChild($iPostEditedByAttr);
							}
							//now we need the summary
						$summary = $post->getContent();
						
						
							//if the post is longer than the length of a summary set in the config, make a substring
							//from the start until position x where x is the length the summary should be, and add "..."
						if(strlen($summary) > ConfigController::getInstance()->getSummaryLength()) {
							$summary=substr($summary, 0, ConfigController::getInstance()->getSummaryLength()) . '...';
						}
						$iPost->appendChild($this->domdocument->createTextNode($summary));
					$myPostsTag->appendChild($iPost);
				}
			}
			else {
				//errorMessage noSuchUser
				$errorMessageTag = $this->domdocument->createElement("errorMessage");
					$errorMessageTypeAttr = $this->domdocument->createAttribute("type");
					$errorMessageTypeAttr->value = "noSuchUser";
				$errorMessageTag->appendChild($errorMessageTypeAttr);
			$pageSpecific->appendChild($errorMessageTag);
			}
		}
		else {
			if(isset($_SESSION['user'])) {
				//list the posts of the user logged in
							$posts = PostController::getInstance()->getPostsByUserName($_SESSION['user']);
				
										foreach($posts as $post) {
					$iPost = $this->domdocument->createElement("iPost");
						$iPostIdAttr = $this->domdocument->createAttribute("id");
						$iPostIdAttr->value = $post->getId();
							$iPost->appendChild($iPostIdAttr);
						$iPostTitleAttr = $this->domdocument->createAttribute("title");
						$iPostTitleAttr->value = $post->getTitle();
							$iPost->appendChild($iPostTitleAttr);
						$iPostAuthorAttr = $this->domdocument->createAttribute("author");
						$iPostAuthorAttr->value = $post->getUser();
							$iPost->appendChild($iPostAuthorAttr);
							
							if($post->getPicture()) {
						$iPostPictureAttr = $this->domdocument->createAttribute("picture");
						$iPostPictureAttr->value = $post->getPicture();
							$iPost->appendChild($iPostPictureAttr);
							}
							
						$iPostCreatedAtAttr = $this->domdocument->createAttribute("createdAt");
						$iPostCreatedAtAttr->value = $post->getCreatedAt();
							$iPost->appendChild($iPostCreatedAtAttr);

						$iPostStatusAttr = $this->domdocument->createAttribute("status");
						$iPostStatusAttr->value = $post->getStatus();
							$iPost->appendChild($iPostStatusAttr);
						
							
							if(!($post->getCreatedAt() == $post->getEditedAt())) {
						$iPostEditedAtAttr = $this->domdocument->createAttribute("editedAt");
						$iPostEditedAtAttr->value = $post->getEditedAt();
							$iPost->appendChild($iPostEditedAtAttr);
						$iPostEditedByAttr = $this->domdocument->createAttribute("editedBy");
						$iPostEditedByAttr->value = $post->getEditedBy();
							$iPost->appendChild($iPostEditedByAttr);
							}
							//now we need the summary
						$summary = $post->getContent();
						
						
							//if the post is longer than the length of a summary set in the config, make a substring
							//from the start until position x where x is the length the summary should be, and add "..."
						if(strlen($summary) > ConfigController::getInstance()->getSummaryLength()) {
							$summary=substr($summary, 0, ConfigController::getInstance()->getSummaryLength()) . '...';
						}
						$iPost->appendChild($this->domdocument->createTextNode($summary));
					$myPostsTag->appendChild($iPost);
				}
				
			}
			else {
				//errorMessage noUserSpecified
				$errorMessageTag = $this->domdocument->createElement("errorMessage");
					$errorMessageTypeAttr = $this->domdocument->createAttribute("type");
					$errorMessageTypeAttr->value = "noUserSpecified";
				$errorMessageTag->appendChild($errorMessageTypeAttr);
			$pageSpecific->appendChild($errorMessageTag);				
			}
		}
	}
	
	public function getUpdatedDOM() {
		return $this->domdocument;
	}
}
?>