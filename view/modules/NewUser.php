<?php

/*
 * post username firstname lastname email password.If the user of the system is an admin, he can set the new user to be admin through role
 * errorMessage
 * type=requiredFieldsMissing
 * type mailAlreadyUsed
 * type=usernameTaken
 */

require_once 'controller/UserController.php';
require_once 'view/OutputterInterface.php';
require_once 'model/User.php';

class NewUser implements OutputterInterface {
	private $domdocument;
	
	function __construct($domdocument) {
		$this->domdocument = $domdocument;
		//first, get the pageSpecific tag
		$pageSpecificAsList = $domdocument->getElementsByTagName('pageSpecific');
		$pageSpecific = $pageSpecificAsList->item(0);
		//create the module tag
		$module = $this->domdocument->createElement("module");
		$pageSpecific->appendChild($module);
		
		//this is the NewUser module, so lets go right ahead and create that tag
		$newUserTag = $this->domdocument->createElement("NewUser");
		$module->appendChild($newUserTag);
		
		if(isset($_POST['username'])) {
			//new user was posted
			if(!empty($_POST['username']) && !empty($_POST['firstname'])
			 && !empty($_POST['lastname']) && !empty($_POST['email']) && !empty($_POST['password'])) {
				//all required fields are filled in.
				if(!UserController::getInstance()->userExists($_POST['username'])) {
					//there is no such user from before
					if(!UserController::getInstance()->emailExists($_POST['email'])) {
						//the e-mail adress has not been used. Create a new user
						//TODO Captcha here
						if(isset($_SESSION['user']) && $_SESSION['userRole'] == 'ADMIN' && isset($_POST['role']) && !empty($_POST['role'])) {
							//the user is an admin, so check if the new user is to be an admin
							if($_POST['role'] == 'ADMIN') {
								$role = 'ADMIN';
							}
							else {
								$role = 'USER';
							}
						}
						else {
							$role = 'USER';
						}
						$user = new User($_POST['username'], null, $_POST['email'], false, $role, null, $_POST['firstname'], 
						$_POST['lastname']);
						//password is taken in account in here
						UserController::getInstance()->newUser($user);
					}
					else {
						//the e-mail has been used before errorMessage mailAlreadyUsed showSchema true
				$errorMessageTag = $this->domdocument->createElement("errorMessage");
					$errorMessageTypeAttr = $this->domdocument->createAttribute("type");
					$errorMessageTypeAttr->value = "mailAlreadyUsed";
				$errorMessageTag->appendChild($errorMessageTypeAttr);
			$pageSpecific->appendChild($errorMessageTag);				
				//showSchema true
			$showSchema = $this->domdocument->createAttribute("showSchema");
			$showSchema->value = "true";
			$newUserTag->appendChild($showSchema);							
					}
				}
				else {
					//the username is taken errorMessage usernameTaken showSchema true
				$errorMessageTag = $this->domdocument->createElement("errorMessage");
					$errorMessageTypeAttr = $this->domdocument->createAttribute("type");
					$errorMessageTypeAttr->value = "usernameTaken";
				$errorMessageTag->appendChild($errorMessageTypeAttr);
			$pageSpecific->appendChild($errorMessageTag);				
				//showSchema true
			$showSchema = $this->domdocument->createAttribute("showSchema");
			$showSchema->value = "true";
			$newUserTag->appendChild($showSchema);						
				}
			}
			else {
				//required fields were missing errorMessage requiredFieldsMissing
				$errorMessageTag = $this->domdocument->createElement("errorMessage");
					$errorMessageTypeAttr = $this->domdocument->createAttribute("type");
					$errorMessageTypeAttr->value = "requiredFieldsMissing";
				$errorMessageTag->appendChild($errorMessageTypeAttr);
			$pageSpecific->appendChild($errorMessageTag);				
				//showSchema true
			$showSchema = $this->domdocument->createAttribute("showSchema");
			$showSchema->value = "true";
			$newUserTag->appendChild($showSchema);		
			}
		}
		else {
			//nothing was posted, showSchema true
			$showSchema = $this->domdocument->createAttribute("showSchema");
			$showSchema->value = "true";
			$newUserTag->appendChild($showSchema);			
		}
	}
	
	function getUpdatedDOM() {
		return $this->domdocument;
	}
}
?>