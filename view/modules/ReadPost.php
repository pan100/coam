<?php

/*
 * give the link an id in a get or this will not work
 */

require_once 'view/OutputterInterface.php';
require_once 'controller/PostController.php';
require_once 'controller/PictureController.php';

class ReadPost implements OutputterInterface {
	private $domdocument;
	
	public function __construct($domdocument) {
		$this->domdocument = $domdocument;
		//first, get the pageSpecific tag
		$pageSpecificAsList = $domdocument->getElementsByTagName('pageSpecific');
		$pageSpecific = $pageSpecificAsList->item(0);
		//create the module tag
		$module = $this->domdocument->createElement("module");
		$pageSpecific->appendChild($module);
		
		//this is the ReadPost module, so lets go right ahead and create that tag
		$readPostTag = $this->domdocument->createElement("ReadPost");
		$module->appendChild($readPostTag);
		
		//if an id was not provided, make an errorMessage
		
		if(!isset($_GET['id'])) {
			$errorMessageTag = $this->domdocument->createElement("errorMessage");
				$errorMessageTypeAttr = $this->domdocument->createAttribute("type");
				$errorMessageTypeAttr->value = "idNotSpecified";
				$errorMessageTag->appendChild($errorMessageTypeAttr);
			$module->appendChild($errorMessageTag);
		}
		
		else {
			if($post = PostController::getInstance()->getPostById($_GET['id'])) {
				//add the correct post
				$postTag = $this->domdocument->createElement("post");
					$postIdAttr = $this->domdocument->createAttribute("id");
					$postIdAttr->value = $post->getId();
				$postTag->appendChild($postIdAttr);
					$postCategoryAttr = $this->domdocument->createAttribute("category");
					$postCategoryAttr->value = $post->getCategory();
				$postTag->appendChild($postCategoryAttr);
					$postTitleAttr = $this->domdocument->createAttribute("title");
					$postTitleAttr->value = $post->getTitle();
				$postTag->appendChild($postTitleAttr);
					$postAuthorAttr = $this->domdocument->createAttribute("author");
					$postAuthorAttr->value = $post->getUser();
				$postTag->appendChild($postAuthorAttr);
					if($post->getPicture()) {
						$postPictureAttr = $this->domdocument->createAttribute("picture");
						$postPictureAttr->value = $post->getPicture();
					$postTag->appendChild($postPictureAttr);
					}
					$postCreatedAtAttr = $this->domdocument->createAttribute("createdAt");
					$postCreatedAtAttr->value = $post->getCreatedAt();
				$postTag->appendChild($postCreatedAtAttr);
					if($post->getCreatedAt() != $post->getEditedAt()) {
						$postEditedAtAttr = $this->domdocument->createAttribute("editedAt");
						$postEditedAtAttr->value = $post->getEditedAt();
					$postTag->appendChild($postEditedAtAttr);
						$postEditedByAttr = $this->domdocument->createAttribute("editedBy");
						$postEditedByAttr->value = $post->getEditedBy();
					$postTag->appendChild($postEditedByAttr);
					}
				$postTag->appendChild($this->domdocument->createTextNode($post->getContent()));
				$readPostTag->appendChild($postTag);
				//then all pictures associated to the post
					$pictures = PictureController::getInstance()->getPicturesByPost($post);
						foreach($pictures as $picture) {
							$pictureTag = $this->domdocument->createElement("picture");
								$pictureFileNameAttr = $this->domdocument->createAttribute("fileName");
								$pictureFileNameAttr->value = $picture->getFileName();
							$pictureTag->appendChild($pictureFileNameAttr);
								$pictureTitleAttr = $this->domdocument->createAttribute("title");
								$pictureTitleAttr->value = $picture->getTitle();
							$pictureTag->appendChild($pictureTitleAttr);
								$pictureUploadedByAttr = $this->domdocument->createAttribute("uploadedBy");
								$pictureUploadedByAttr->value = $picture->getUploadedBy();
							$pictureTag->appendChild($pictureUploadedByAttr);
								$pictureLastChangedAttr = $this->domdocument->createAttribute("lastChanged");
								$pictureLastChangedAttr->value = $picture->getLastChanged();
							$pictureTag->appendChild($pictureLastChangedAttr);
								$pictureDescriptionAttr = $this->domdocument->createAttribute("description");
								$pictureDescriptionAttr->value = $picture->getDescription();
							$pictureTag->appendChild($pictureDescriptionAttr);
							
							$readPostTag->appendChild($pictureTag);
						}
				//then the comment hierarchy (NOT IMPLEMENTED)
				
			}
			else {
				//there were no such post. Make an errorMessage
				$errorMessageTag = $this->domdocument->createElement("errorMessage");
				$errorMessageTypeAttr = $this->domdocument->createAttribute("type");
				$errorMessageTypeAttr->value = "noSuchPost";
				$errorMessageTag->appendChild($errorMessageTypeAttr);
				$errorMessageAdditionalInfoTag = $this->domdocument->createAttribute("additionalInfo");
				$errorMessageAdditionalInfoTag->value = $_GET['id'];
				$errorMessageTag->appendChild($errorMessageAdditionalInfoTag);
			$module->appendChild($errorMessageTag);
				
			}
		}
		
	}
	
	function getUpdatedDOM() {
		return $this->domdocument;
	}
}
?>