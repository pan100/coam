<?php

require_once 'view/OutputterInterface.php';
class ShowCategories implements OutputterInterface {
		private $domdocument;
		
			public function __construct($domdocument) {
		$this->domdocument = $domdocument;
		//first, get the pageSpecific tag
		$pageSpecificAsList = $domdocument->getElementsByTagName('pageSpecific');
		$pageSpecific = $pageSpecificAsList->item(0);
		//create the module tag
		$module = $this->domdocument->createElement("module");
		$pageSpecific->appendChild($module);
		
		//this is the ShowCategories module, so lets go right ahead and create that tag
		$showCategoriesTag = $this->domdocument->createElement("ShowCategories");
		$module->appendChild($showCategoriesTag);
		
		//the ShowCategories tag is EMPTY in the DTD and has no attributes. FINISHED!
			}
	
	public function getUpdatedDOM() {
		return $this->domdocument;
	}
}
?>