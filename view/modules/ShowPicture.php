<?php
require_once 'view/OutputterInterface.php';
require_once 'controller/PictureController.php';
require_once 'model/Post.php';

/*
 * get - filename=[the filename]
 * errorMessage pictureNotFound
 */

class ShowPicture implements OutputterInterface {
		private $domdocument;
	
	public function __construct($domdocument) {
		$this->domdocument = $domdocument;
		//first, get the pageSpecific tag
		$pageSpecificAsList = $domdocument->getElementsByTagName('pageSpecific');
		$pageSpecific = $pageSpecificAsList->item(0);
		//create the module tag
		$module = $this->domdocument->createElement("module");
		$pageSpecific->appendChild($module);
		
		//this is the ShowPicture module, so lets go right ahead and create that tag
		$showPictureTag = $this->domdocument->createElement("ShowPicture");
		$module->appendChild($showPictureTag);
		
		if(isset($_GET['filename'])) {
			if($picture = PictureController::getInstance()->getPictureByFilename($_GET['filename'])) {
				//create the picture tag
								$pictureTag = $this->domdocument->createElement("picture");
								$pictureFileNameAttr = $this->domdocument->createAttribute("fileName");
								$pictureFileNameAttr->value = $picture->getFileName();
							$pictureTag->appendChild($pictureFileNameAttr);
								$pictureTitleAttr = $this->domdocument->createAttribute("title");
								$pictureTitleAttr->value = $picture->getTitle();
							$pictureTag->appendChild($pictureTitleAttr);
								$pictureUploadedByAttr = $this->domdocument->createAttribute("uploadedBy");
								$pictureUploadedByAttr->value = $picture->getUploadedBy();
							$pictureTag->appendChild($pictureUploadedByAttr);
								$pictureLastChangedAttr = $this->domdocument->createAttribute("lastChanged");
								$pictureLastChangedAttr->value = $picture->getLastChanged();
							$pictureTag->appendChild($pictureLastChangedAttr);
								$pictureDescriptionAttr = $this->domdocument->createAttribute("description");
								$pictureDescriptionAttr->value = $picture->getDescription();
							$pictureTag->appendChild($pictureDescriptionAttr);
							
							$showPictureTag->appendChild($pictureTag);
			}
			else {
				//the picture was not found in the database, so it cannot be shown. errorMessage pictureNotFound
				
							$errorMessageTag = $this->domdocument->createElement("errorMessage");
				$errorMessageTypeAttr = $this->domdocument->createAttribute("type");
				$errorMessageTypeAttr->value = "pictureNotFound";
				$errorMessageTag->appendChild($errorMessageTypeAttr);
			$module->appendChild($errorMessageTag);
			}
		}
	}
	
	public function getUpdatedDOM() {
		return $this->domdocument;
	}
}
?>