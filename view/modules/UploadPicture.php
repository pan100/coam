<?php
/*
 * We user POST values name, description and picture. Picture contains the file.
 */

require_once 'controller/PictureController.php';
require_once 'view/OutputterInterface.php';

class UploadPicture implements OutputterInterface {
	private $domdocument;
	
	public function __construct($domdocument) {
		$this->domdocument = $domdocument;
		
					//first, get the pageSpecific tag
		$pageSpecificAsList = $domdocument->getElementsByTagName('pageSpecific');
		$pageSpecific = $pageSpecificAsList->item(0);
		//create the module tag
		$module = $this->domdocument->createElement("module");
		$pageSpecific->appendChild($module);
		
		//this is the EditUser module, so lets go right ahead and create that tag
		$uploadPictureTag = $this->domdocument->createElement("UploadPicture");
		$module->appendChild($uploadPictureTag);
		
		if(isset($_POST['name'])) {
			if(!empty($_POST['name']) && !empty($_POST['description']) && !empty($_FILES['picture'])) {
				if(PictureController::getInstance()->addPicture($_POST['name'], $_POST['description'])) {
					//infoMessage pictureUploaded
			$infoMessageTag = $this->domdocument->createElement("infoMessage");
			$infoMessageTypeAttr = $this->domdocument->createAttribute("type");
			$infoMessageTypeAttr->value = "pictureUploaded";
			$infoMessageTag->appendChild($infoMessageTypeAttr);
			$pageSpecific->appendChild($infoMessageTag);
					//showSchema = false
			$showSchema = $this->domdocument->createAttribute("showSchema");
			$showSchema->value = "false";
			$uploadPictureTag->appendChild($showSchema);
				}
				else {
					//errorMessage pictureUploadFailed
				$errorMessageTag = $this->domdocument->createElement("errorMessage");
					$errorMessageTypeAttr = $this->domdocument->createAttribute("type");
					$errorMessageTypeAttr->value = "pictureUploadFailed";
				$errorMessageTag->appendChild($errorMessageTypeAttr);
			$pageSpecific->appendChild($errorMessageTag);
					//showSchema = true			
			$showSchema = $this->domdocument->createAttribute("showSchema");
			$showSchema->value = "true";
			$uploadPictureTag->appendChild($showSchema);			
			}
		}
		
		else {
			//errorMessage requiredInputMissing
				$errorMessageTag = $this->domdocument->createElement("errorMessage");
					$errorMessageTypeAttr = $this->domdocument->createAttribute("type");
					$errorMessageTypeAttr->value = "requiredInputMissing";
				$errorMessageTag->appendChild($errorMessageTypeAttr);
			$pageSpecific->appendChild($errorMessageTag);
			//showSchema = true
			$showSchema = $this->domdocument->createAttribute("showSchema");
			$showSchema->value = "true";
			$uploadPictureTag->appendChild($showSchema);	
		}
		}
		
	}
	
	public function getUpdatedDOM() {
		return $this->domdocument;
	}
}
?>