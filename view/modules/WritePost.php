<?php
//if get attribute "category" is set, this should be preselected in the form, because it then comes from a write post link in a
//specific category NOT IMPLEMENTED IN THIS VERSION BECAUSE OF TROUBLE WITH XSLT VS XHTML

/*
 * errorMessages:
 * type=notAuthorized
 */

require_once 'view/OutputterInterface.php';
require_once 'model/Post.php';
require_once 'controller/PostController.php';

class WritePost implements OutputterInterface {
	private $domdocument;
	
	public function __construct($domdocument) {
		$this->domdocument = $domdocument;
		
				//first, get the pageSpecific tag
		$pageSpecificAsList = $domdocument->getElementsByTagName('pageSpecific');
		$pageSpecific = $pageSpecificAsList->item(0);
		//create the module tag
		$module = $this->domdocument->createElement("module");
		$pageSpecific->appendChild($module);
		
		//this is the UserInfo module, so lets go right ahead and create that tag
		$writePostTag = $this->domdocument->createElement("WritePost");
		$module->appendChild($writePostTag);
		
		//if the user is not logged in errorMessage notAuthorized
		if(!isset($_SESSION['user'])) {
							$errorMessageTag = $this->domdocument->createElement("errorMessage");
				$errorMessageTypeAttr = $this->domdocument->createAttribute("type");
				$errorMessageTypeAttr->value = "notAuthorized";
				$errorMessageTag->appendChild($errorMessageTypeAttr);
			$pageSpecific->appendChild($errorMessageTag);
		}
		
		else {
			//the user is logged on and authorized to write a post
			
			if(isset($_POST['title']) || isset($_POST['content']) || isset($_POST['status'])) {
				//one of the required fields was provided if one is provided, all must be provided
				if(!(!empty($_POST['title']) && !empty($_POST['content']) && !empty($_POST['status']))) {
					//if one of the required fields is missing, errorMessage invalidInput
							$errorMessageTag = $this->domdocument->createElement("errorMessage");
				$errorMessageTypeAttr = $this->domdocument->createAttribute("type");
				$errorMessageTypeAttr->value = "invalidInput";
				$errorMessageTag->appendChild($errorMessageTypeAttr);
			$pageSpecific->appendChild($errorMessageTag);					
				}
				else {
					//all required fields were provided. Because of a lack of time, we just assume the data is ok, and make the
					//post
					//TODO check the input
					$picture = null;
					if(!empty($_POST['picture'])) {
						$picture = $_POST['picture'];
					}
					PostController::getInstance()->newPost($_POST['title'], $_POST['content'], $_POST['status'], $picture, $_SESSION['user'], $_POST['category']);
					
							$infoMessageTag = $this->domdocument->createElement("infoMessage");
						$infoMessageTypeAttr = $this->domdocument->createAttribute("type");
						$infoMessageTypeAttr->value = "postSaved";
					$infoMessageTag->appendChild($infoMessageTypeAttr);
					$pageSpecific->appendChild($infoMessageTag);
				}
			}
		}
		
		
	}
	
	public function getUpdatedDOM() {
		return $this->domdocument;
	}
}
?>